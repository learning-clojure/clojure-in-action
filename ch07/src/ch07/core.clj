(ns ch07.core
  (:gen-class))

;; 7 Evolving Clojure through macros
;; 7.1 Macro basics
;; 7.1.1 Textual substitution

(def a-ref (ref 0))
;=> #'ch07.core/a-ref

(dosync
  (ref-set a-ref 1))
;=> 1

(defmacro sync-set [r v]
  (list 'dosync
        (list 'ref-set r v)))
;=> #'ch07.core/sync-set

(sync-set a-ref 1)
;=> 1

;; 7.1.2 The unless example

; if form
;(if test then else)

(defn exhibits-oddity? [x]
  (if (odd? x)
    (println "Very odd!")))
;=> #'ch07.core/exhibits-oddity?

; if Clojure would have unless it could be written as following:
;(defn exhibits-oddity? [x]
;  (unless (even? x)
;          (println "Very odd, indeed!")))

;; THE UNLESS FUNCTION
(defn unless [test then]
  (if (not test)
    then))
;=> #'ch07.core/unless
;(defn exhibits-oddity? [x]
;  (unless (even? x)
;          (println "Very odd, indeed!")))

(defn exhibits-oddity? [x]
  (unless (even? x)
          (println "Very odd, indeed!")))
;=> #'ch07.core/exhibits-oddity?
(exhibits-oddity? 11)
;Very odd, indeed!
;=> nil
(exhibits-oddity? 10)
;Very odd, indeed!
;=> nil

; workaround
(defn unless [test then-thunk]
  (if (not test)
    (then-thunk)))
;=> #'ch07.core/unless

(defn exhibits-oddity? [x]
  (unless (even? x)
          #(println "Rather odd!")))
;=> #'ch07.core/exhibits-oddity?

(exhibits-oddity? 11)
;Rather odd!
;=> nil
(exhibits-oddity? 10)
;=> nil

;; THE UNLESS MACRO
(defmacro unless [test then]
  (list 'if (list 'not test)
        then))
;=> #'ch07.core/unless

(defn exhibits-oddity? [x]
  (unless (even? x)
          (println "Very odd, indeed!")))
;=> #'ch07.core/exhibits-oddity?

(exhibits-oddity? 11)
;Very odd, indeed!
;=> nil
(exhibits-oddity? 10)
;=> nil

(macroexpand
  '(unless (even? x) (println "Very odd, indeed!")))
;=> (if (not (even? x)) (println "Very odd, indeed!"))

;; 7.1.3 Macro templates
;; TEMPLATING USING THE BACKQUOTE (`) MACRO

(defmacro unless [test then]
  `(if (not ~test)
     ~then))
;=> #'ch07.core/unless

;; UNQUOTING
(defmacro unless [test then]
  `(if (not ~test)
     then))
;=> #'ch07.core/unless
(macroexpand '(unless (even? x) (println "Very odd, indeed!")))
;=> (if (clojure.core/not (even? x)) ch07.core/then)

;; SPLICING
(defmacro unless [test then]
  `(if (not ~test)
     ~then))

(defn exhibits-oddity? [x]
  (unless (even? x)
          (println "Odd!")
          (println "Very odd!")))
;CompilerException clojure.lang.ArityException: Wrong number of args (3) passed to: core/unless, compiling:(/Users/luhtonen/development/projects/clojure/clojure-in-action/ch07/src/ch07/core.clj:118:3)
; unless accepts only 1 parameter

(defn exhibits-oddity? [x]
  (unless (even? x)
          (do
            (println "Odd!")
            (println "Very odd!"))))
;=> #'ch07.core/exhibits-oddity?

; improved unless macro
(defmacro unless [test & exprs]
  `(if (not ~test)
     (do ~exprs)))
;=> #'ch07.core/unless
(exhibits-oddity? 11)
;Odd!
;Very odd!
;=> nil

(macroexpand-1 '(unless (even? x)
                        (println "Odd!")
                        (println "Very odd!")))
;=> (if (clojure.core/not (even? x)) (do ((println "Odd!") (println "Very odd!"))))

;; UNQUOTE SPLICE READER MACRO (~@)
(defmacro unless [test & exprs]
  `(if (not ~test)
     (do ~@exprs)))
;=> #'ch07.core/unless
(exhibits-oddity? 11)
;Odd!
;Very odd!
;=> nil

;; GENERATING NAMES
(defmacro def-logged-fn [fn-name args & body]
  `(defn ~fn-name ~args
     (let [now (System/currentTimeMillis)]
       (println "[" now "] Call to" (str (var ~fn-name)))
       ~@body)))
;=> #'ch07.core/def-logged-fn

(def-logged-fn printname [name]
               (println "hi" name))
;CompilerException java.lang.RuntimeException: Can't let qualified name: ch07.core/now, compiling:(/Users/luhtonen/development/projects/clojure/clojure-in-action/ch07/src/ch07/core.clj:164:1)

(macroexpand-1 '(def-logged-fn printname [name]
                               (println "hi" name)))
;=>
;(clojure.core/defn
;  printname
;  [name]
;  (clojure.core/let
;    [ch07.core/now (java.lang.System/currentTimeMillis)]
;    (clojure.core/println "[" ch07.core/now "] Call to" (clojure.core/str (var printname)))
;    (println "hi" name)))

; fix with reader macro #
(defmacro def-logged-fn [fn-name args & body]
  `(defn ~fn-name ~args
     (let [now# (System/currentTimeMillis)]
       (println "[" now# "] Call to" (str (var ~fn-name)))
       ~@body)))
;=> #'ch07.core/def-logged-fn
(def-logged-fn printname [name]
               (println "hi" name))
;=> #'ch07.core/printname

(printname "deepthi")
;[ 1485762899895 ] Call to #'ch07.core/printname
;hi deepthi
;=> nil

;; 7.2 Macros from within Clojure
;; 7.2.2 declare
(macroexpand  '(declare add multiply subtract divide))
;=> (do (def add) (def multiply) (def subtract) (def divide))

;; 7.2.3 defonce
(macroexpand '(defonce x 1))
;=> (let* [v__5590__auto__ (def x)] (clojure.core/when-not (.hasRoot v__5590__auto__) (def x 1)))

;; 7.2.4 and
(macroexpand '(and (even? x) (> x 50) (< x 500)))
;=> (let* [and__4467__auto__ (even? x)] (if and__4467__auto__ (clojure.core/and (> x 50) (< x 500)) and__4467__auto__))

;; 7.2.5 time
(time (* 1331 13531))
;"Elapsed time: 0.190249 msecs"
;=> 18009761

(macroexpand '(time (* 1331 13531)))
;=>
;(let*
;  [start__5057__auto__ (. java.lang.System (clojure.core/nanoTime)) ret__5058__auto__ (* 1331 13531)]
;  (clojure.core/prn
;    (clojure.core/str
;      "Elapsed time: "
;      (clojure.core//
;        (clojure.core/double (clojure.core/- (. java.lang.System (clojure.core/nanoTime)) start__5057__auto__))
;        1000000.0)
;      " msecs"))
;  ret__5058__auto__)

;; 7.3 Writing your own macros
;; 7.3.1 infix
(defmacro infix [expr]
  (let [[left op right] expr]
    (list op left right)))
;=> #'ch07.core/infix
(infix (3 + 2))
;=> 5

;; 7.3.2 randomly
(defmacro randomly [& exprs]
  (let [len (count exprs)
        index (rand-int len)
        conditions (map #(list '= index %) (range len))]
    `(cond ~@(interleave conditions exprs))))
;=> #'ch07.core/randomly
(randomly (println "amit") (println "deepthi") (println "adi"))
;amit
;=> nil
(randomly (println "amit") (println "deepthi") (println "adi"))
;adi
;=> nil

(macroexpand-1
  '(randomly (println "amit") (println "deepthi") (println "adi")))
;=> (clojure.core/cond (= 1 0) (println "amit") (= 1 1) (println "deepthi") (= 1 2) (println "adi"))

(defmacro randomly-2 [& exprs]
  (nth exprs (rand-int (count exprs))))
;=> #'ch07.core/randomly-2
(randomly-2 (println "amit") (println "deepthi") (println "adi"))
;deepthi
;=> nil
(randomly-2 (println "amit") (println "deepthi") (println "adi"))
;amit
;=> nil

(defmacro randomly-3 [& exprs]
  (let [c (count exprs)]
    `(case (rand-int ~c) ~@(interleave (range c) exprs))))
(randomly-3 (println "amit") (println "deepthi") (println "adi"))
;amit
;=> nil
(randomly-3 (println "amit") (println "deepthi") (println "adi"))
;deepthi
;=> nil

;; 7.3.3 defwebmethod

(defn check-credentials [username password]
  true)
;=> #'ch07.core/check-credentials

(defn login-user [request]
  (let [username (:username request)
        password (:password request)]
    (if (check-credentials username password)
      (str "Welcome back, " username ", " password " is correct!")
      (str "Login failed!"))))
;=> #'ch07.core/login-user

(def request {:username "amit" :password "123456"})
;=> #'user/request
(login-user request)
;=> "Welcome back, amit, 123456 is correct!"

;; improving code with macro

(defmacro defwebmethod [name args & exprs]
  `(defn ~name [{:keys ~args}]
     ~@exprs))
;=> #'ch07.core/defwebmethod

(defwebmethod login-user [username password]
              (if (check-credentials username password)
                (str "Welcome, " username ", " password " is still correct!")
                (str "Login failed!")))
;=> #'ch07.core/login-user
(login-user request)
;=> "Welcome, amit, 123456 is still correct!"

;; 7.3.4 defnn
(defmacro defnn [fname [& names] & body]
  (let [ks {:keys (vec names)}]
    `(defn ~fname [& {:as arg-map#}]
       (let [~ks arg-map#]
         ~@body))))
;=> #'ch07.core/defnn
(defnn print-details [name salary start-date]
       (println "Name:" name)
       (println "Salary:" salary)
       (println "Started on:" start-date))
;=> #'ch07.core/print-details
(print-details :start-date "10/22/2009" :name "Rob" :salary 1000000)
;Name: Rob
;Salary: 1000000
;Started on: 10/22/2009
;=> nil
(macroexpand-1 '(print-details :start-date "10/22/2009" :name "Rob" :salary 1000000))
;=> (print-details :start-date "10/22/2009" :name "Rob" :salary 1000000)

;; 7.3.5 assert-true
(defmacro assert-true [test-expr]
  (let [[operator lhs rhs] test-expr]
    `(let [rhsv# ~rhs ret# ~test-expr]
       (if-not ret#
         (throw (RuntimeException.
                  (str '~lhs " is not " '~operator " " rhsv#)))
         true))))
;=> #'ch07.core/assert-true
(assert-true (= (* 2 4) (/ 16 2)))
;=> true
(assert-true (< (* 2 4) (/ 18 2)))
;=> true
(assert-true (>= (* 2 4) (/ 18 2)))
;RuntimeException (* 2 4) is not >= 9  ch07.core/eval2424 (form-init6201968284237540620.clj:1)
(macroexpand-1 '(assert-true (>= (* 2 4) (/ 18 2))))
;=>
;(clojure.core/let
;  [rhsv__2396__auto__ (/ 18 2) ret__2397__auto__ (>= (* 2 4) (/ 18 2))]
;  (clojure.core/if-not
;    ret__2397__auto__
;    (throw (java.lang.RuntimeException. (clojure.core/str (quote (* 2 4)) " is not " (quote >=) " " rhsv__2396__auto__)))
;    true))

; improved version
(defmacro assert-true [test-expr]
  (if-not (= 3 (count test-expr))
    (throw (RuntimeException.
             "Argument must be of the form
                   (operator test-expr expected-expr)")))
  (if-not (some #{(first test-expr)} '(< > <= >= = not=))
    (throw (RuntimeException.
             "operator must be one of < > <= >= = not=")))
  (let [[operator lhs rhs] test-expr]
    `(let [rhsv# ~rhs ret# ~test-expr]
       (if-not ret#
         (throw (RuntimeException.
                  (str '~lhs " is not " '~operator " " rhsv#)))
         true))))
;=> #'ch07.core/assert-true
(assert-true (>= (* 2 4) (/ 18 2) (+ 2 5)))
;RuntimeException Argument must be of the form
;                   (operator test-expr expected-expr)  ch07.core/assert-true (core.clj:353)
(assert-true (<> (* 2 4) (/ 16 2)))
;RuntimeException operator must be one of < > <= >= = not=  ch07.core/assert-true (core.clj:357)
