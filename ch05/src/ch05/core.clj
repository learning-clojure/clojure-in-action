(ns ch05.core
  (:gen-class))

;; 5.  Exploring Clojure and Java interop
;; 5.1 Calling Java from Clojure
;; 5.1.1 Importing Java classes into Clojure

(import 'java.util.Date 'java.text.SimpleDateFormat)
(import '[java.util Date Set])
(ns com.clojureinaction.book
  (:import (java.util Set Date)))

;; 5.1.2 Creating instances

(import '(java.text SimpleDateFormat))
(def sdf (new SimpleDateFormat "yyyy-MM-dd"))
;=> #'ch05.core/sdf
;; Clojure idiomatic way to do the same
(def sdf (SimpleDateFormat. "yyyy-MM-dd"))
;=> #'ch05.core/sdf

;; 5.1.3 Accessing methods and fields
(defn date-from-date-string [date-string]
  (let [sdf (SimpleDateFormat. "yyyy-MM-dd")]
    (.parse sdf date-string)))
;=> #'ch05.core/date-from-date-string

;; STATIC METHODS

(Long/parseLong "12321")
;=> 12321

;; STATIC FIELDS
(import '(java.util Calendar))
;=> java.util.Calendar
Calendar/JANUARY
;=> 0
Calendar/FEBRUARY
;=> 1

;; 5.1.4 Macros and the dot special form

(. System getenv "PATH")
(. System (getenv "PATH"))

(import '(java.util Random))
;=> java.util.Random
(def rnd (Random.))
;=> #'user/rnd
(. rnd nextInt 10)
;=> 4
(. rnd (nextInt 10))
;=> 8

(. Calendar DECEMBER)
;=> 11

;; .. (DOT-DOT)

(import '(java.util Calendar TimeZone))
;=> java.util.TimeZone
(. (. (Calendar/getInstance) (getTimeZone)) (getDisplayName))
;=> "Central European Time"

; same with less parenthensis
(. (. (Calendar/getInstance) getTimeZone) getDisplayName)
;=> "Central European Time"

; and even simpler with double dots
(.. (Calendar/getInstance) getTimeZone getDisplayName)
;=> "Central European Time"

(.. (Calendar/getInstance)
    getTimeZone
    (getDisplayName true TimeZone/SHORT))
;=> "CEST"

;; DOTO

(import '(java.util Calendar))
(defn the-past-midnight-1 []
  (let [calendar-obj (Calendar/getInstance)]
    (.set calendar-obj Calendar/AM_PM Calendar/AM)
    (.set calendar-obj Calendar/HOUR 0)
    (.set calendar-obj Calendar/MINUTE 0)
    (.set calendar-obj Calendar/SECOND 0)
    (.set calendar-obj Calendar/MILLISECOND 0)
    (.getTime calendar-obj)))
;=> java.util.Calendar
;=> #'ch05.core/the-past-midnight-1

; same with doto macro
(defn the-past-midnight-2 []
  (let [calendar-obj (Calendar/getInstance)]
    (doto calendar-obj
      (.set Calendar/AM_PM Calendar/AM)
      (.set Calendar/HOUR 0)
      (.set Calendar/MINUTE 0)
      (.set Calendar/SECOND 0)
      (.set Calendar/MILLISECOND 0))
    (.getTime calendar-obj)))
;=> #'ch05.core/the-past-midnight-2

;; 5.1.5 Helpful Clojure macros for working with Java
;; MEMFN

(map (fn [x] (.getBytes x)) ["amit" "rob" "kyle"])
;=>
;(#object["[B" 0x24045e17 "[B@24045e17"]
;  #object["[B" 0x44f5924c "[B@44f5924c"]
;  #object["[B" 0x2f8b1c14 "[B@2f8b1c14"])

; same simplified
(map #(.getBytes %) ["amit" "rob" "kyle"])
;=>
;(#object["[B" 0x1cdd7ae6 "[B@1cdd7ae6"]
;  #object["[B" 0x65ec5f35 "[B@65ec5f35"]
;  #object["[B" 0x7b685163 "[B@7b685163"])

; example of memfn usage
(memfn getBytes)
;=>
;#object[com.clojureinaction.book$eval1437$fn__1438
;        0x76b5e422
;        "com.clojureinaction.book$eval1437$fn__1438@76b5e422"]

; with type hints
(memfn ^String getBytes)
;=> #object[ch05.core$eval1444$fn__1445 0x3ea86874 "ch05.core$eval1444$fn__1445@3ea86874"]

(map (memfn getBytes) ["amit" "rob" "kyle"])
;=>
;(#object["[B" 0x103bd0dd "[B@103bd0dd"]
;  #object["[B" 0x799a0599 "[B@799a0599"]
;  #object["[B" 0x2104f281 "[B@2104f281"])

(.subSequence "Clojure" 2 5)
;=> "oju"

; same with type hints
((memfn ^String subSequence ^Long start ^Long end) "Clojure" 2 5)
;=> "oju"

;; BEAN

(bean (Calendar/getInstance))
;=>
;{:weeksInWeekYear 52,
; :timeZone #object[sun.util.calendar.ZoneInfo
;                   0x5c14efb3
;                   "sun.util.calendar.ZoneInfo[id=\"Europe/Zurich\",offset=3600000,dstSavings=3600000,useDaylight=true,transitions=119,lastRule=java.util.SimpleTimeZone[id=Europe/Zurich,offset=3600000,dstSavings=3600000,useDaylight=true,startYear=0,startMode=2,startMonth=2,startDay=-1,startDayOfWeek=1,startTime=3600000,startTimeMode=2,endMode=2,endMonth=9,endDay=-1,endDayOfWeek=1,endTime=3600000,endTimeMode=2]]"],
; :weekDateSupported true,
; :weekYear 2017,
; :lenient true,
; :time #inst"2017-01-26T06:28:43.690-00:00",
; :calendarType "gregory",
; :timeInMillis 1485412123690,
; :class java.util.GregorianCalendar,
; :firstDayOfWeek 1,
; :gregorianChange #inst"1582-10-15T00:00:00.000-00:00",
; :minimalDaysInFirstWeek 1}

;; ARRAYS

(def tokens (.split "clojure.in.action" "\\."))
;=> #'ch05.core/tokens
tokens
;=> #object["[Ljava.lang.String;" 0x45a7e38f "[Ljava.lang.String;@45a7e38f"]
(alength tokens)
;=> 3
(aget tokens 2)
;=> "action"
(aset tokens 2 "actionable")
;=> "actionable"
tokens
;=> #object["[Ljava.lang.String;" 0x45a7e38f "[Ljava.lang.String;@45a7e38f"]

;; 5.1.6 Implementing interfaces and extending classes

(import 'java.awt.event.MouseAdapter)
(proxy [MouseAdapter] []
  (mousePressed [event]
    (println "Hey!")))
;=>
;#object[ch05.core.proxy$java.awt.event.MouseAdapter$ff19274a
;        0x61c902ed
;        "ch05.core.proxy$java.awt.event.MouseAdapter$ff19274a@61c902ed"]

;; REIFY

(reify java.io.FileFilter
  (accept [this f]
    (.isDirectory f)))
;=> #object[ch05.core$eval1517$reify__1518 0x25bd479d "ch05.core$eval1517$reify__1518@25bd479d"]

;; 5.2 Compiling Clojure code to Java bytecode
;; 5.2.1 Example: A tale of two calculators

(compile 'org.elu.utils.calculators)
;=> org.elu.utils.calculators

;; 5.2.2 Creating Java classes and interfaces using gen-class and gen-interface

;; 5.3 Calling Clojure from Java
