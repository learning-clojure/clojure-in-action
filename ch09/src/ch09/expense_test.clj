(ns ch09.expense-test
  (:import [org.elu.expenses Expense])
  (:require [ch09.expense :refer :all]
            [clojure.test :refer :all]))

(def clj-expenses [(new-expense "2009-8-20" 21 95 "books" "amazon.com")
                   (new-expense "2009-8-21" 72 43 "food" "mollie-stones")
                   (new-expense "2009-8-22" 315 71 "car-rental" "avis")
                   (new-expense "2009-8-23" 15 68 "books" "borders")])

(deftest test-clj-expenses-total
  (is (= 42577 (total-amount clj-expenses)))
  (is (=  3763 (total-amount (category-is "books") clj-expenses))))

(def java-expenses [(Expense. "2009-8-24" 44 95 "books" "amazon.com")
                    (Expense. "2009-8-25" 29 11 "gas" "shell")])

(deftest test-java-expenses-total
  (let [total-cents (map #(.amountInCents %) java-expenses)]
    (is (= 7406 (apply + total-cents)))))

(def mixed-expenses (concat clj-expenses java-expenses))

;; This test will fail with clj-in-act.ch9.expense
(deftest test-mixed-expenses-total
  (is (= 49983 (total-amount mixed-expenses)))
  (is (= 8258 (total-amount (category-is "books") mixed-expenses))))

;; to run the test
;(run-tests 'ch09.expense-test)
;
;Testing ch09.expense-test
;
;Ran 1 tests containing 2 assertions.
;0 failures, 0 errors.
;=> {:test 1, :pass 2, :fail 0, :error 0, :type :summary}
