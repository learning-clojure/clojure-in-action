(ns ch09.core
  (:require [ch09.modus-operandi :refer :all])
  (:gen-class))

;; 9 Protocols, records, and types
;; 9.1 The expression problem
;; 9.1.3 Clojure’s multimethods solution
; mutlimethod solves the problem, but have some issues

;; 9.2 Examining the operations side of the expression problem

;; load file in repl src/ch09/modus_operandi.clj
(def-modus-operandi ExpenseCalculations
                    (total-cents [e])
                    (is-category? [e category]))
;=> nil
ExpenseCalculations
;=> {:total-cents {:args [e]}, :is-category? {:args [e category]}}

;; 9.3 Examining the data types side of the expression problem with protocols
;; 9.3.1 defprotocol and extend-protocol

;; 9.3.2 Defining data types with deftype, defrecord, and reify
;; DEFRECORD

(defrecord NewExpense [date amount-dollars amount-cents category merchant-name])
;=> ch09.core.NewExpense
(NewExpense. "2010-04-01" 29 95 "gift" "1-800-flowers")
;=>
;#ch09.core.NewExpense{:date "2010-04-01",
;                      :amount-dollars 29,
;                      :amount-cents 95,
;                      :category "gift",
;                      :merchant-name "1-800-flowers"}

(defrecord Foo [a b])
;=> ch09.core.Foo
(def foo (->Foo 1 2))
;=> #'ch09.core/foo
(assoc foo :extra-key 3)
;=> #ch09.core.Foo{:a 1, :b 2, :extra-key 3}
(dissoc (assoc foo :extra-key 3) :extra-key)
;=> #ch09.core.Foo{:a 1, :b 2}
(dissoc foo :a)
;=> {:b 2}
(foo :a)
;ClassCastException ch09.core.Foo cannot be cast to clojure.lang.IFn  ch09.core/eval1604 (form-init5561938392920988021.clj:3)

;; DEFTYPE
(deftype Mytype [a b])
; => ch09.core.Mytype
; this generates following Java class
;public final class Mytype {
;  public final Object a;
;  public final Object b;
;  public Mytype(Object obj, Object obj1) {
;    a = obj;
;    b = obj1;
;  }
;}

;; REIFY
(defn new-expense [date-string dollars cents category merchant-name]
  (let [calendar-date (Calendar/getInstance)]
    (.setTime calendar-date
              (.parse (SimpleDateFormat. "yyyy-MM-dd") date-string))
    (reify ExpenseCalculations
      (total-cents [this]
        (-> dollars
            (* 100)
            (+ cents)))
      (is-category? [this some-category]
        (= category some-category)))))
