(ns ch10.core
  (:gen-class))

;; 10 Test-driven development and more
;; 10.1 Getting started with TDD: Manipulating dates in strings
;; 10.1.1 First assertion

; to run tests execute following command: lein test

; first run result:
;Exception in thread "main" java.io.FileNotFoundException: Could not locate ch10/date_operations__init.class or ch10/date_operations.clj on classpath. Please check that namespaces with dashes use underscores in the Clojure file name., compiling:(ch10/date_operations_spec.clj:1:1)

; after creating namespace without implementation
;Exception in thread "main" java.lang.RuntimeException: Unable to resolve symbol: date in this context, compiling:(ch10/date_operations_spec.clj:6:11)

; after adding empty implementations
;FAIL in (test-simple-data-parsing) (date_operations_spec.clj:7)
;expected: (= (day-from d) 22)
;  actual: (not (= nil 22))

(import '(java.text SimpleDateFormat))
;=> java.text.SimpleDateFormat
(def f (SimpleDateFormat. "yyyy-MM-dd"))
;=> #'ch10.core/f
(.parse f "2010-08-15")
;=> #inst"2010-08-14T22:00:00.000-00:00"

(import '(java.util GregorianCalendar))
;=> java.util.GregorianCalendar
(def gc (GregorianCalendar.))
;=> #'ch10.core/gc
(def d (.parse f "2010-08-15"))
;=> #'ch10.core/d
(.setTime gc d)
;=> nil

(load "date_operations")
;=> nil
(ch10.date-operations/date "2010-08-15")
;=> #inst"2010-08-15T00:00:00.000+02:00"

(import '(java.util Calendar))
;=> java.util.Calendar
(.get gc Calendar/DAY_OF_MONTH)
;=> 15

;; after adding final implementation tests are passing

;; 10.1.2 month-from and year-from

;; 10.1.3 as-string

;; 10.1.4 Incrementing and decrementing
(load "date_operations")
(def d (ch10.date-operations/date "2009-10-31"))
;=> #'ch10.core/d
(.add d Calendar/DAY_OF_MONTH 1)
;=> nil
(ch10.date-operations/as-string d)
;=> "2009-11-01"

;; 10.1.5 Refactor mercilessly

;; 10.2 Improving tests through mocking and stubbing
;; 10.2.1 Example: Expense finders

;; 10.2.2 Stubbing
(defmacro stubbing [stub-forms & body]
  (let [stub-pairs (partition 2 stub-forms)
        returns (map last stub-pairs)
        stub-fn (map #(list 'constantly %) returns)
        real-fn (map first stub-pairs)]
    `(with-redefs [~@(interleave real-fn stub-fn)]
       ~@body)))
;=> #'ch10.core/stubbing

(defn calc-x [x1 x2]
  (* x1 x2))
;=> #'ch10.core/calc-x
(defn calc-y [y1 y2]
  (/ y2 y1))
;=> #'ch10.core/calc-y
(defn some-client []
  (println (calc-x 2 3) (calc-y 3 4)))
;=> #'ch10.core/some-client
(some-client)
;6 4/3
;=> nil
(stubbing [calc-x 1
           calc-y 2]
          (some-client))
;1 2
;=> nil
(macroexpand-1 '(stubbing [calc-x 1 calc-y 2]
                          (some-client)))
;=> (clojure.core/with-redefs [calc-x (constantly 1) calc-y (constantly 2)] (some-client))

;; 10.2.3 Mocking
;; 10.2.4 Mocks versus stubs

;; 10.3 Organizing tests
;; 10.3.1 The testing macro
;; 10.3.2 The are macro
(:require '[clojure.test :refer :all])
(defn to-upper [s]
  (.toUpperCase (str s)))
(deftest test-to-upcase
         (is (= "RATHORE" (to-upper "rathore")))
         (is (= "1" (to-upper 1)))
         (is (= "AMIT" (to-upper "amit"))))

;; removing duplication with `are` macro
(deftest test-to-upcase
         (are [l u] (= u (to-upper l))
              "RATHORE" "rathore"
              "1"       "1"
              "AMIT"    "amit"))
