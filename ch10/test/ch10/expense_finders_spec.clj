(ns ch10.expense-finders-spec
  (:require [ch10.expense-finders :refer :all]
            [ch10.stubbing :refer :all]
            [clojure.test :refer :all]))

(def all-expenses [{:amount 10.0 :date "2010-02-28"}
                   {:amount 20.0 :date "2010-02-25"}
                   {:amount 30.0 :date "2010-02-21"}])

(defmocktest test-fetch-expenses-greater-than
             (stubbing [fetch-all-expenses all-expenses]
                       (let [filtered (fetch-expenses-greater-than "" "" "" 15.0)]
                         (is (= (count filtered) 2))
                         (is (= (:amount (first filtered)) 20.0))
                         (is (= (:amount (last filtered)) 30.0)))))

(defmocktest test-filter-greater-than
             (mocking [log-call]
                      (let [filtered (expenses-greater-than all-expenses 15.0)]
                        (testing "the filtering itself works as expected"
                          (is (= (count filtered) 2))
                          (is (= (:amount (first filtered)) 20.0))
                          (is (= (:amount (last filtered)) 30.0))))
                      (testing "Auditing via log-call works correctly"
                        (verify-call-times-for log-call 1)
                        (verify-first-call-args-for log-call "expenses-greater-than" 15.0))))
