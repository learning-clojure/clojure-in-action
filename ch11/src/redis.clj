(ns redis
  "Mock Redis library for code listings in chapter 11"
  (:refer-clojure :exclude [set get])
  (:import (java.util.concurrent ConcurrentHashMap ConcurrentMap)))

;;; Private

(defonce ^ConcurrentMap connections (ConcurrentHashMap.))

(defn get-setting-default [^ConcurrentMap m k vfn]
  (if-some [v (.get m k)]
    v
    (let [newv (vfn)]
      (if-some [v (.putIfAbsent m k newv)]
        v
        newv))))

(defn clear-connections! []
  (.clear connections))

;;; Public API

(def ^:dynamic ^ConcurrentMap *connection*)

(defn set [key value]
  (.put *connection* key value)
  "OK")

(defn get [key]
  (.get *connection* key))

(defmacro with-server [{:keys [host]} & body]
  `(binding [*connection* (get-setting-default connections ~host
                            #(ConcurrentHashMap.))]
     ~@body))
