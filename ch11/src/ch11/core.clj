(ns ch11.core
  (:gen-class)
  (:require redis)
  (:use ch11.engine
        ch11.segment
        ch11.session
        ch11.dsl-store
        clojure.walk))

;; 11 More macros and DSLs
;; 11.1 A quick review of macros
(defmacro single-arg-fn [binding-form & body]
  `((fn [~(first binding-form)] ~@body) ~(second binding-form)))
;=> #'ch11.core/single-arg-fn
(defmacro my-let [lettings & body]
  (if (empty? lettings)
    `(do ~@body)
    `(single-arg-fn ~(take 2 lettings)
                    (my-let ~(drop 2 lettings) ~@body))))
;=> #'ch11.core/my-let
(my-let [[a b] [2 5]
         {:keys [x y]} {:x (* a b) :y 20}
         z (+ x y)]
        (println "a,b,x,y,z:" a b x y z)
        (* x y z))
;a,b,x,y,z: 2 5 10 20 30
;=> 6000

;; 11.2 Anaphoric macros
;; 11.2.1 The anaphoric if
(defn some-computation [x]
  (if (even? x) false (inc x)))
;=> #'ch11.core/some-computation

;; IMPLEMENTING ANAPHORIC-IF
(defmacro anaphoric-if [test-form then-form]
  `(if-let [~'it ~test-form]
     ~then-form))
;=> #'ch11.core/anaphoric-if
(macroexpand-1 '(anaphoric-if (some-computation 11)
                              (* 2 it)))
;=> (clojure.core/if-let [it (some-computation 11)] (* 2 it))
(anaphoric-if (some-computation 12)
              (* 2 it))
;=> nil
(anaphoric-if (some-computation 11)
              (* 2 it))
;=> 24

;; GENERALIZING THE ANAPHORIC IF
(defmacro with-it [operator test-form & exprs]
  `(let [~'it ~test-form]
     (~operator ~'it ~@exprs)))
;=> #'ch11.core/with-it
(with-it if (some-computation 12)
         (* 2 it))
;=> nil
(with-it if (some-computation 11)
         (* 2 it))
;=> 24
(with-it and (some-computation 11) (> it 10) (* 2 it))
;=> 24
(with-it when (some-computation 11)
         (println "Got it:" it)
         (* 2 it))
;Got it: 12
;=> 24

;; 11.2.2 The thread-it macro
(defn surface-area-cylinder [r h]
  (-> r
      (+ h)
      (* 2 Math/PI r)))
;=> #'ch11.core/surface-area-cylinder

;; THREADING IN ANY POSITION
; with anonymous function
;(defn another-calculation [a-collection]
;  (->> (seq a-collection)
;       (filter some-pred?)
;       (map a-transform)
;       (#(compute-averages-from % another-pred?))))

; without anonymous function
;(defn another-calculation [a-collection]
;  (as-> (seq a-collection) result
;        (filter some-pred? result)
;        (map a-transform result)
;        (compute-averages-from result another-pred?)))

;; IMPLEMENTING THREAD-IT
; with thread-it macro
;(defn yet-another-calculation [a-collection]
;  (thread-it (seq a-collection)
;             (filter some-pred? it)
;             (map a-transform it)
;             (compute-averages-from it another-pred?)))
(defmacro thread-it [& [first-expr & rest-expr]]
  (if (empty? rest-expr)
    first-expr
    `(let [~'it ~first-expr]
       (thread-it ~@rest-expr))))
;=> #'ch11.core/thread-it
(thread-it (* 10 20) (inc it) (- it 8) (* 10 it) (/ it 5))
;=> 386

;; 11.3 Shifting computation to compile time
;; 11.3.1 Example: Rotation ciphers without macros
;; GENERALIZED ROTATION CIPHERS
(def ALPHABETS [\a \b \c \d \e \f \g \h \i \j \k \l \m \n \o \p \q \r \s \t
                \u \v \w \x \y \z])
;=> #'ch11.core/ALPHABETS
(def NUM-ALPHABETS (count ALPHABETS))
;=> #'ch11.core/NUM-ALPHABETS
(def INDICES (range 1 (inc NUM-ALPHABETS)))
;=> #'ch11.core/INDICES
(def lookup (zipmap INDICES ALPHABETS))
;=> #'ch11.core/lookup

(defn shift [shift-by index]
  (let [shifted (+ (mod shift-by NUM-ALPHABETS) index)]
    (cond
      (<= shifted 0) (+ shifted NUM-ALPHABETS)
      (> shifted NUM-ALPHABETS) (- shifted NUM-ALPHABETS)
      :default shifted)))
;=> #'ch11.core/shift

(shift 10 13)
;=> 23
(shift 20 13)
;=> 7

(defn shifted-tableau [shift-by]
  (->> (map #(shift shift-by %) INDICES)
       (map lookup)
       (zipmap ALPHABETS)))
;=> #'ch11.core/shifted-tableau

(shifted-tableau 13)
;;=>
;{\a \n,
; \b \o,
; \c \p,
; \d \q,
; \e \r,
; \f \s,
; \g \t,
; \h \u,
; \i \v,
; \j \w,
; \k \x,
; \l \y,
; \m \z,
; \n \a,
; \o \b,
; \p \c,
; \q \d,
; \r \e,
; \s \f,
; \t \g,
; \u \h,
; \v \i,
; \w \j,
; \x \k,
; \y \l,
; \z \m}

(defn encrypt [shift-by plaintext]
  (let [shifted (shifted-tableau shift-by)]
    (apply str (map shifted plaintext))))
;=> #'ch11.core/encrypt

(encrypt 13 "abracadabra")
;=> "noenpnqnoen"
(encrypt 13 "noenpnqnoen")
;=> "abracadabra"

(defn decrypt [shift-by encrypted]
  (encrypt (- shift-by) encrypted))
;=> #'ch11.core/decrypt
(decrypt 13 "noenpnqnoen")
;=> "abracadabra"

(def encrypt-with-rot13 (partial encrypt 13))
;=> #'ch11.core/encrypt-with-rot13
(def decrypt-with-rot13 (partial decrypt 13))
;=> #'ch11.core/decrypt-with-rot13

(decrypt-with-rot13 (encrypt-with-rot13 "abracadabra"))
;=> "abracadabra"

;; 10.3.2 Making the compiler work harder
(defn encrypt-with-rot13 [plaintext]
  (apply str (map {\a \n \b \o \c \p} plaintext)))
;=> #'ch11.core/encrypt-with-rot13

(defmacro def-rot-encrypter [name shift-by]
  (let [tableau (shifted-tableau shift-by)]
    `(defn ~name [~'message]
       (apply str (map ~tableau ~'message)))))
;=> #'ch11.core/def-rot-encrypter

(macroexpand-1 '(def-rot-encrypter encrypt13 13))
;=>
;(clojure.core/defn
;  encrypt13
;  [message]
;  (clojure.core/apply
;    clojure.core/str
;    (clojure.core/map
;      {\a \n,
;       \b \o,
;       \c \p,
;       \d \q,
;       \e \r,
;       \f \s,
;       \g \t,
;       \h \u,
;       \i \v,
;       \j \w,
;       \k \x,
;       \l \y,
;       \m \z,
;       \n \a,
;       \o \b,
;       \p \c,
;       \q \d,
;       \r \e,
;       \s \f,
;       \t \g,
;       \u \h,
;       \v \i,
;       \w \j,
;       \x \k,
;       \y \l,
;       \z \m}
;      message)))

(def-rot-encrypter encrypt13 13)
;=> #'ch11.core/encrypt13
(encrypt13 "abracadabra")
;=> "noenpnqnoen"

(defmacro define-rot-encryption [shift-by]
  `(do
     (def-rot-encrypter ~(symbol (str "encrypt" shift-by)) ~shift-by)
     (def-rot-encrypter ~(symbol (str "decrypt" shift-by)) ~(- shift-by))))
;=> #'ch11.core/define-rot-encryption

(define-rot-encryption 15)
;=> #'ch11.core/decrypt15
(encrypt15 "abracadabra")
;=> "pqgprpspqgp"
(decrypt15 "pqgprpspqgp")
;=> "abracadabra"

;; 11.4 Macro-generating macros
(declare x y)
;=> #'ch11.core/y

;; 11.4.1 Example template
(defmacro b [& stuff]
  `(binding ~@stuff))
;=> #'ch11.core/b

;; 11.4.2 Implementing make-synonym
(defmacro synonym [new-name old-name])
;=> #'ch11.core/synonym

(defmacro make-synonym [new-name old-name]
  (defmacro b [& stuff]
    `(binding ~@stuff)))
;=> #'ch11.core/make-synonym
; this won't work

; improved version of macro
(defmacro make-synonym [new-name old-name]
  `(defmacro ~new-name [& stuff]
     `(~old-name ~@stuff)))
;=> #'ch11.core/make-synonym

(macroexpand-1 '(make-synonym b binding))
;=>
;(clojure.core/defmacro
;  b
;  [& ch11.core/stuff]
;  (clojure.core/seq (clojure.core/concat (clojure.core/list ch11.core/old-name) ch11.core/stuff)))

(defmacro back-quote-test []
  `(something))
;=> #'ch11.core/back-quote-test
(macroexpand '(back-quote-test))
;=> (user/something)
(defmacro back-quote-test []
  ``(something))
;=> #'ch11.core/back-quote-test


(macroexpand '(back-quote-test))
;=> (clojure.core/seq (clojure.core/concat (clojure.core/list (quote ch11.core/something))))

(macroexpand-1 '(make-synonym b binding))
;=>
;(clojure.core/defmacro
;  b
;  [& ch11.core/stuff]
;  (clojure.core/seq (clojure.core/concat (clojure.core/list ch11.core/old-name) ch11.core/stuff)))

(defmacro back-quote-test []
  ``(~something))
;=> #'ch11.core/back-quote-test
(macroexpand '(back-quote-test))
;=> (clojure.core/seq (clojure.core/concat (clojure.core/list ch11.core/something)))

(defmacro back-quote-test []
  ``(~@something))
;=> #'ch11.core/back-quote-test
(macroexpand '(back-quote-test))
;=> (clojure.core/seq (clojure.core/concat ch11.core/something))

(defmacro make-synonym [new-name old-name]
  `(defmacro ~new-name [& ~'stuff]
     `(~old-name ~@~'stuff)))
;=> #'ch11.core/make-synonym
(macroexpand-1 '(make-synonym b binding))
;=>
;(clojure.core/defmacro
;  b
;  [& stuff]
;  (clojure.core/seq (clojure.core/concat (clojure.core/list ch11.core/old-name) stuff)))

; final fix
(defmacro make-synonym [new-name old-name]
  `(defmacro ~new-name [& ~'stuff]
     `(~'~old-name ~@~'stuff)))
;=> #'ch11.core/make-synonym
(macroexpand-1 '(make-synonym b binding))
;=>
;(clojure.core/defmacro b [& stuff] (clojure.core/seq (clojure.core/concat (clojure.core/list (quote binding)) stuff)))

(defmacro b [& stuff]
  `(binding ~@stuff))
;=> #'ch11.core/b
(declare x y)
;=> #'ch11.core/y
(make-synonym b binding)
;=> #'ch11.core/b
;(b [x 10 y 20] [x y])
;=> [10 20]

;; 11.5 Domain-specific languages
;; 11.5.2 User classification
;; DATA ELEMENT
{:consumer-id  "abc"
 :url-referrer "http://www.google.com/search?q=clojure+programmers"
 :search-terms ["clojure" "programmers"]
 :ip-address   "192.168.0.10"
 :tz-offset    420
 :user-agent   :safari}
;=>
;{:consumer-id "abc",
; :url-referrer "http://www.google.com/search?q=clojure+programmers",
; :search-terms ["clojure" "programmers"],
; :ip-address "192.168.0.10",
; :tz-offset 420,
; :user-agent :safari}

;; USER SESSION PERSISTENCE
;(def redis-key-for :consumer-id)
;=> #'ch11.core/redis-key-for

;(defn save-session [session]
;  (redis/set (redis-key-for session) (pr-str session)))
;=> #'ch11.core/save-session
;(defn find-session [consumer-id]
;  (read-string (redis/get consumer-id)))
;=> #'ch11.core/find-session

;(def ^:dynamic *session*)
;=> #'ch11.core/*session*

;(defmacro in-session [consumer-id & body]
;  `(binding [*session* (find-session ~consumer-id)]
;     (do ~@body)))
;=> #'ch11.core/in-session

;; SEGMENTING USERS
; some examples
; segment 1
;(defsegment googling-clojurians
;            (and
;              (> (count $search-terms) 0)
;              (matches? $url-referrer "google")))
;segment 2
;(defsegment loyal-safari
;            (and
;              (empty? $url-referrer)
;              (= :safari $user-agent)))

; macro skeleton
;(defmacro defsegment [segment-name & body])
;=> #'ch11.core/defsegment

; support functions
;(defn drop-first-char [name]
;  (apply str (rest name)))
;=> #'ch11.core/drop-first-char
;(defn session-lookup [dollar-name]
;  (->> (drop-first-char dollar-name)
;       (keyword)
;       (list '*session*)))
;=> #'ch11.core/session-lookup

;(defn transform-lookups [dollar-attribute]
;  (let [prefixed-string (str dollar-attribute)]
;    (if-not (.startsWith prefixed-string "$")
;      dollar-attribute
;      (session-lookup prefixed-string))))
;=> #'ch11.core/transform-lookups
; usage
(transform-lookups '$user-agent)
;=> (*session* :user-agent)

(postwalk transform-lookups '(> (count $search-terms) 0))
;=> (> (count (*session* :search-terms)) 0)

;(defmacro defsegment [segment-name & body]
;  (let [transformed (postwalk transform-lookups body)]))
;=> #'ch11.core/defsegment

(postwalk transform-lookups '(and
                               (> (count $search-terms) 0)
                               (= :safari $user-agent)))
;=> (and (> (count (*session* :search-terms)) 0) (= :safari (*session* :user-agent)))

;(defmacro defsegment [segment-name & body]
;  (let [transformed (postwalk transform-lookups body)]
;    `(let [segment-fn# (fn [] ~@transformed)]
;       (register-segment ~(keyword segment-name) segment-fn#))))
;=> #'ch11.core/defsegment

;(defsegment loyal-safari
;            (and
;              (empty? $url-referrer)
;              (= :safari $user-agent)))
;=>
;{:segments {:loyal-safari #object[ch11.core$eval3716$segment_fn__2669__auto____3717
;                                  0x4035b59a
;                                  "ch11.core$eval3716$segment_fn__2669__auto____3717@4035b59a"]}}

;; ADDING PRIMITIVES TO THE EXECUTION ENGINE
;(defn matches? [^String superset ^String subset]
;  (and
;    (not (empty? superset))
;    (> (.indexOf superset subset) 0)))

(def dsl-code (str
                '(defsegment googling-clojurians
                             (and
                               (> (count $search-terms) 0)
                               (matches? $url-referrer "google")))
                '(defsegment loyal-safari
                             (and
                               (empty? $url-referrer)
                               (= :safari $user-agent)))))
;=> #'ch11.core/dsl-code
(load-code dsl-code)
;=>
;{:segments {:loyal-safari #object[ch11.engine$eval4230$segment_fn__2669__auto____4231
;                                  0x3b9481e1
;                                  "ch11.engine$eval4230$segment_fn__2669__auto____4231@3b9481e1"],
;            :googling-clojurians #object[ch11.engine$eval4224$segment_fn__2669__auto____4225
;                                         0x2968e7f6
;                                         "ch11.engine$eval4224$segment_fn__2669__auto____4225@2968e7f6"]}}

(def abc-session {
                  :consumer-id "abc"
                  :url-referrer "http://www.google.com/search?q=clojure+programmers"
                  :search-terms ["clojure" "programmers"]
                  :ip-address "192.168.0.10"
                  :tz-offset 480
                  :user-agent :safari})
;=> #'ch11.core/abc-session

(redis/with-server {:host "localhost"}
                   (save-session abc-session))
;=> "OK"

; testing implementation
(redis/with-server {:host "localhost"}
                   (in-session "abc"
                               (println "The current user is in:" (classify))))
;The current user is in: (:googling-clojurians)

;; INCREASING COMBINABILITY
;(defsegment googling-clojurians-chrome
;            (and
;              (> (count $search-terms) 0)
;              (matches? $url-referrer "google")
;              (= :chrome $user-agent)))
;=> #'ch11.core/googling-clojurians-chrome

; same with increased macro removes duplication
(defsegment googling-clojurians-chrome
            (and
              (googling-clojurians)
              (= :chrome $user-agent)))
;=> #'ch11.core/googling-clojurians-chrome

;; DYNAMIC UPDATES
(load-code (str '(defsegment googling-clojurians
                             (and
                               (> (count $search-terms) 0)
                               (matches? $url-referrer "yahoo")))))
;=> #'ch11.engine/googling-clojurians
