(ns ch08.core
  (:gen-class))

;; 8 More on functional programming
;; 8.1 Using higher-order functions
;; 8.1.1 Collecting results of functions

(defn square [x]
  (* x x))
;=> #'ch08.core/square
(defn square-all [numbers]
  (if (empty? numbers)
    nil
    (cons (square (first numbers))
          (square-all (rest numbers)))))
;=> #'ch08.core/square-all
(square-all [1 2 3 4 5 6])
;=> (1 4 9 16 25 36)

(defn cube [x]
  (* x x x))
;=> #'ch08.core/cube
(defn cube-all [numbers]
  (if (empty? numbers)
    ()
    (cons (cube (first numbers))
          (cube-all (rest numbers)))))
;=> #'ch08.core/cube-all
(cube-all [1 2 3 4 5 6])
;=> (1 8 27 64 125 216)

; introducing higher order function
(defn do-to-all [f numbers]
  (if (empty? numbers)
    ()
    (cons (f (first numbers))
          (do-to-all f (rest numbers)))))
;=> #'ch08.core/do-to-all
(do-to-all square [1 2 3 4 5 6])
;=> (1 4 9 16 25 36)
(do-to-all cube [1 2 3 4 5 6])
;=> (1 8 27 64 125 216)

; but it have a flow
(do-to-all square (range 11000))
;StackOverflowError   clojure.core/seq--4357 (core.clj:137)

; revised implementation
(defn do-to-all [f numbers]
  (lazy-seq
    (if (empty? numbers)
      ()
      (cons (f (first numbers))
            (do-to-all f (rest numbers))))))
;=> #'ch08.core/do-to-all
(take 10 (drop 10000 (do-to-all square (range 11000))))
;=> (100000000 100020001 100040004 100060009 100080016 100100025 100120036 100140049 100160064 100180081)

; similar with the map function
(take 10 (drop 10000 (map square (range 11000))))
;=> (100000000 100020001 100040004 100060009 100080016 100100025 100120036 100140049 100160064 100180081)

;; 8.1.2 Reducing lists of things

(defn total-of [numbers]
  (loop [nums numbers sum 0]
    (if (empty? nums)
      sum
      (recur (rest nums) (+ sum (first nums))))))
;=> #'ch08.core/total-of
(total-of [5 7 9 3 4 1 2 8])
;=> 39

(defn larger-of [x y]
  (if (> x y) x y))
;=> #'ch08.core/larger-of

(defn largest-of [numbers]
  (loop [l numbers candidate (first numbers)]
    (if (empty? l)
      candidate
      (recur (rest l) (larger-of candidate (first l))))))
;=> #'ch08.core/largest-of
(largest-of [5 7 9 3 4 1 2 8])
;=> 9
(largest-of [])
;=> nil

; extracting the commonality
(defn compute-across [func elements value]
  (if (empty? elements)
    value
    (recur func (rest elements) (func value (first elements)))))
;=> #'ch08.core/compute-across
(defn total-of [numbers]
  (compute-across + numbers 0))
;=> #'ch08.core/total-of
(defn largest-of [numbers]
  (compute-across larger-of numbers (first numbers)))
;=> #'ch08.core/largest-of
(total-of [5 7 9 3 4 1 2 8])
;=> 39
(largest-of [5 7 9 3 4 1 2 8])
;=> 9

(defn all-greater-than [threshold numbers]
  (compute-across #(if (> %2 threshold) (conj %1 %2) %1) numbers []))
;=> #'ch08.core/all-greater-than
(all-greater-than 5 [5 7 9 3 4 1 2 8])
;=> [7 9 8]

; same rewritten using the built-in reduce
(defn all-greater-than [threshold numbers]
  (reduce #(if (> %2 threshold) (conj %1 %2) %1) [] numbers))
;=> #'ch08.core/all-greater-than
(all-greater-than 5 [5 7 9 3 4 1 2 8])
;=> [7 9 8]

;; 8.1.3 Filtering lists of things

(defn all-lesser-than [threshold numbers]
  (compute-across #(if (< %2 threshold) (conj %1 %2) %1) numbers []))
;=> #'ch08.core/all-lesser-than
(all-lesser-than 5 [5 7 9 3 4 1 2 8])
;=> [3 4 1 2]

; extracting the common part
(defn select-if [pred elements]
  (compute-across #(if (pred %2) (conj %1 %2) %1) elements []))
;=> #'ch08.core/select-if
(select-if odd? [5 7 9 3 4 1 2 8])
;=> [5 7 9 3 1]

; rewriting previous function
(defn all-lesser-than [threshold numbers]
  (select-if #(< % threshold) numbers))
;=> #'ch08.core/all-lesser-than

; same with filter function
(filter odd? [57934128])
;=> (5 7 9 3 1)
;=> ()

;; 8.2 Partial application
;; 8.2.1 Adapting functions

(defn price-with-tax [tax-rate amount]
  (->> (/ tax-rate 100)
       (+ 1)
       (* amount)))
;=> #'ch08.core/price-with-tax
(price-with-tax 9.5M 100)
;=> 109.500M

(defn with-california-taxes [prices]
  (map #(price-with-tax 9.25M %) prices))
;=> #'ch08.core/with-california-tax
(def prices [100 200 300 400 500])
;=> #'ch08.core/prices
(with-california-taxes prices)
;=> (109.2500M 218.5000M 327.7500M 437.0000M 546.2500M)

(defn price-calculator-for-tax [state-tax]
  (fn [price]
    (price-with-tax state-tax price)))
;=> #'ch08.core/price-calculator-for-tax
(def price-with-ca-tax (price-calculator-for-tax 9.25M))
;=> #'ch08.core/price-with-ca-tax
(def price-with-ny-tax (price-calculator-for-tax 8.0M))
;=> #'ch08.core/price-with-ny-tax

;; PARTIAL APPLICATION
(defn of-n-args [a b c d e]
  (str a b c d e ))
;=> #'ch08.core/of-n-args
(defn of-k-args [d e]
  (of-n-args 1 2 3 d e))
;=> #'ch08.core/of-k-args
(of-k-args \a \b)
;=> "123ab"

(defn partially-applied [of-n-args & n-minus-k-args]
  (fn [& k-args]
    (apply of-n-args (concat n-minus-k-args k-args))))
;=> #'ch08.core/partially-applied
(def of-2-args (partially-applied of-n-args \a \b \c))
;=> #'ch08.core/of-2-args
(def of-3-args (partially-applied of-n-args \a \b))
;=> #'ch08.core/of-3-args
(of-2-args 4 5)
;=> "abc45"
(of-3-args 3 4 5)
;=> "ab345"

; same with partial function
(def of-2-args (partial of-n-args \a \b \c))
;=> #'ch08.core/of-2-args
(def of-3-args (partial of-n-args \a \b))
;=> #'ch08.core/of-3-args
(of-2-args 4 5)
;=> "abc45"
(of-3-args 3 4 5)
;=> "ab345"

;; 8.2.2 Defining functions
(defn select-into-if [container pred elements]
  (compute-across #(if (pred %2) (conj %1 %2) %1) elements container))
;=> #'ch08.core/select-into-if
(def numbers [4 9 5 7 6 3 8])
;=> #'ch08.core/numbers
(select-into-if [] #(< % 7) numbers)
;=> [4 5 6 3]
(select-into-if () #(< % 7) numbers)
;=> (3 6 5 4)

(def select-up (partial select-into-if []))
;=> #'ch08.core/select-up
(def select-down (partial select-into-if ()))
;=> #'ch08.core/select-down
(select-up #(< % 9)[5 3 9 6 8])
;=> [5 3 6 8]
(select-down  #(< % 9) [5 3 9 6 8])
;=> (8 6 3 5)

;; 8.3 Closures
;; 8.3.1 Free variables and closures

(defn adder [num1 num2]
  (let [x (+ num1 num2)]
    (fn [y]
      (+ x y)))) ;x is a free variable: its value will come from outside scope of function that uses it.
;=> #'ch08.core/adder
(def add-5 (adder 2 3))
;=> #'ch08.core/add-5
(add-5 10)
;=> 15

;; 8.3.2 Delayed computation and closures

(let [x 1 y 0]
  (/ x y))
;CompilerException java.lang.ArithmeticException: Divide by zero, compiling:(/Users/luhtonen/development/projects/clojure/clojure-in-action/ch08/src/ch08/core.clj:240:1)

(let [x 1
      y 0]
  (try
    (/ x y)
    (catch Exception e (println (.getMessage e)))))
;Divide by zero
;=> nil

(defn try-catch [the-try the-catch]
  (try
    (the-try)
    (catch Exception e (the-catch e))))
;=> #'ch08.core/try-catch
(let [x 1
      y 0]
  (try-catch #(/ x y)
             #(println (.getMessage %))))
;Divide by zero
;=> nil

;; 8.3.3 Closures and objects

(defn new-user [login password email]
  (fn [a]
    (case a
      :login login
      :password password
      :email email
      nil)))
;=> #'ch08.core/new-user
(def arjun (new-user "arjun" "secret" "arjun@zololabs.com"))
;=> #'ch08.core/arjun
(arjun :login)
;=> "arjun"
(arjun :password)
;=> "secret"
(arjun :email)
;=> "arjun@zololabs.com"
(arjun :name)
;=> nil

(defn new-user [login password email]
  (fn [a]
    (case a
      :login login
      :email email
      :password-hash (hash password)
      nil)))
;=> #'ch08.core/new-user
(def arjun (new-user "arjun" "secret" "arjun@zololabs.com"))
;=> #'ch08.core/arjun
(arjun :password)
;=> nil
(arjun :password-hash)
;=> 1614358358

;; DATA OR FUNCTION?
(defn new-user [login password email]
  (fn [a & args]
    (case a
      :login login
      :email email
      :authenticate (= password (first args)))))
;=> #'ch08.core/new-user
(def adi (new-user "adi" "secret" "adi@currylogic.com"))
;=> #'ch08.core/adi
(adi :authenticate "blah")
;=> false
(adi :authenticate "secret")
;=> true

;; 8.3.4 An object system for Clojure

;(defclass Person)
;(Person :name)

(defn new-class [class-name]
  (fn [command & args]
    (case command
      :name (name class-name))))
;=> #'ch08.core/new-class
(defmacro defclass [class-name]
  `(def ~class-name (new-class '~class-name)))
;=> #'ch08.core/defclass

(defclass Person)
;=> #'user/Person
(Person :name)
;=> "Person"

(def some-class Person)
;=> #'ch08.core/some-class
(some-class :name)
;=> "Person"

;; CREATING INSTANCES
(defn new-object [klass]
  (fn [command & args]
    (case command
      :class klass)))
;=> #'ch08.core/new-object
(def cindy (new-object Person))
;=> #'ch08.core/cindy
(new-object Person)
;=> #object[ch08.core$new_object$fn__2061 0x7067fe4d "ch08.core$new_object$fn__2061@7067fe4d"]
((cindy :class) :name)
;=> "Person"

(defn new-object [klass]
  (fn [command & args]
    (case command
      :class klass
      :class-name (klass :name))))
;=> #'ch08.core/new-object
(def cindy (new-object Person))
;=> #'ch08.core/cindy
(cindy :class-name)
;=> "Person"

(defn new-class [class-name]
  (fn klass [command & args]
    (case command
      :name (name class-name)
      :new (new-object klass))))
;=> #'ch08.core/new-class
(defclass Person)
;=> #'ch08.core/Person
(def nancy (Person :new))
;=> #'ch08.core/nancy
(nancy :class-name)
;=> "Person"

;; OBJECTS AND STATE
(defn new-object [klass]
  (let [state (ref {})]
    (fn [command & args]
      (case command
        :class klass
        :class-name (klass :name)
        :set! (let [[k, v] args]
                (dosync (alter state assoc k v))
                nil)
        :get (let [[key] args]
               (@state key))))))
;=> #'ch08.core/new-object
(defclass Person)
;=> #'ch08.core/Person
(def nancy (Person :new))
;=> #'ch08.core/nancy
(nancy :get :name)
;=> nil
(nancy :set! :name "Nancy Drew")
;=> nil
(nancy :get :name)
;=> "Nancy Drew"

;; DEFINING METHODS
(defn method-spec [sexpr]
  (let [name (keyword (second sexpr))
        body (next sexpr)]
    [name (conj body 'fn)]))
;=> #'ch08.core/method-spec

(method-spec '(method age [] (* 2 10)))
;=> [:age (fn age [] (* 2 10))]

(defn method-specs [sexprs]
  (->> sexprs
       (filter #(= 'method (first %)))
       (mapcat method-spec)
       (apply hash-map)))
;=> #'ch08.core/method-specs
(method-specs '((method age [] (* 2 10))
                 (method greet [visitor] (str "Hello there, " visitor))))

;=> {:age (fn age [] (* 2 10)), :greet (fn greet [visitor] (str "Hello there, " visitor))}
(defn new-class [class-name methods]
  (fn klass [command & args]
    (case command
      :name (name class-name)
      :new (new-object klass))))
;=> #'ch08.core/new-class
(defmacro defclass [class-name & specs]
  (let [fns (or (method-specs specs) {})]
    `(def ~class-name (new-class '~class-name ~fns))))
;=> #'ch08.core/defclass
(defclass Person
          (method age [] (* 2 10))
          (method greet [visitor] (str "Hello there, " visitor)))
;=> #'ch08.core/Person

;; INVOKING METHODS

(defn find-method [method-name instance-method]
  (instance-method method-name))
;=> #'ch08.core/find-method
(defn new-class [class-name methods]
  (fn klass [command & args]
    (case command
      :name (name class-name)
      :new (new-object klass)
      :method (let [[method-name] args]
                (find-method method-name methods)))))
;=> #'ch08.core/new-class
(defclass Person
          (method age [] (* 2 10))
          (method greet [visitor] (str "Hello there, " visitor)))
;=> #'ch08.core/Person
(Person :method :age)
;=> #object[ch08.core$age__2304 0x7edc784a "ch08.core$age__2304@7edc784a"]
((Person :method :age))
;=> 20

; improved version
(defn new-object [klass]
  (let [state (ref {})]
    (fn [command & args]
      (case command
        :class klass
        :class-name (klass :name)
        :set! (let [[k v] args]
                (dosync (alter state assoc k v))
                nil)
        :get (let [[key] args]
               (@state key))
        (if-let [method (klass :method command)]
          (apply method args)
          (throw (RuntimeException.
                   (str "Unable to respond to " command))))))))
;=> #'ch08.core/new-object
(defclass Person
          (method age [] (* 2 10))
          (method greet [visitor] (str "Hello there, " visitor)))
;=> #'ch08.core/Person
(def shelly (Person :new))
;=> #'ch08.core/shelly
(shelly :age)
;=> 20
(shelly :greet "Nancy")
;=> "Hello there, Nancy"

;; REFERRING TO THIS
(declare ^:dynamic this)
;=> #'ch08.core/this
(defn new-object [klass]
  (let [state (ref {})]
    (fn thiz [command & args]
      (case command
        :class klass
        :class-name (klass :name)
        :set! (let [[k v] args]
                (dosync (alter state assoc k v))
                nil)
        :get (let [[key] args] (@state key))
        (let [method (klass :method command)]
          (if-not method
            (throw (RuntimeException.
                     (str "Unable to respond to " command))))
          (binding [this thiz]
            (apply method args)))))))
;=> #'ch08.core/new-object
(defclass Person
          (method age [] (* 2 10))
          (method greet [visitor] (str "Hello there, " visitor))
          (method about [diff]
                  (str "I was born about " (+ diff (this :age)) " years ago")))
;=> #'ch08.core/Person
(def shelly (Person :new))
;=> #'ch08.core/shelly
(shelly :about 2)
;=> "I was born about 22 years ago"

;; CLASS INHERITANCE
(defn parent-class-spec [sexprs]
  (let [extends-spec (filter #(= 'extends (first %)) sexprs)
        extends (first extends-spec)]
    (if (empty? extends)
      'OBJECT
      (last extends))))
;=> #'ch08.core/parent-class-spec
(parent-class-spec '((extends Person)
                      (method age [] (* 2 9))))
;=> Person
(defmacro defclass [class-name & specs]
  (let [parent-class (parent-class-spec specs)
        fns (or (method-specs specs) {})]
    `(def ~class-name (new-class '~class-name  #'~parent-class ~fns))))
;=> #'ch08.core/defclass
(defn new-class [class-name parent methods]
  (fn klass [command & args]
    (case command
      :name (name class-name)
      :parent parent
      :new (new-object klass)
      :method (let [[method-name] args]
                (find-method method-name methods)))))
;=> #'ch08.core/new-class
(def OBJECT (new-class :OBJECT nil {}))
;=> #'ch08.core/OBJECT
(defclass Person
          (method age [] (* 2 10))
          (method about [diff]
                  (str "I was born about " (+ diff (this :age)) " years ago")))
;=> #'ch08.core/Person
(defclass Woman
          (extends Person)
          (method greet [v] (str "Hello, " v))
          (method age [] (* 2 9)))
;=> #'ch08.core/Woman

; but this still have a problem
(def donna (Woman :new))
;=> #'ch08.core/donna
(donna :greet "Shelly")
;=> "Hello, Shelly"
(donna :age)
;=> 18
(donna :about 3)
;RuntimeException Unable to respond to :about  ch08.core/new-object/thiz--2390 (core.clj:501)

(defn find-method [method-name klass]
  (or ((klass :methods) method-name)
      (if-not (= #'OBJECT klass)
        (find-method method-name (klass :parent)))))
;=> #'ch08.core/find-method
(defn new-class [class-name parent methods]
  (fn klass [command & args]
    (case command
      :name (name class-name)
      :parent parent
      :new (new-object klass)
      :methods methods
      :method (let [[method-name] args]
                (find-method method-name klass)))))
;=> #'ch08.core/new-class
(defclass Person
          (method age [] (* 2 10))
          (method about [diff]
                  (str "I was born about " (+ diff (this :age)) " years ago")))
;=> #'ch08.core/Person
(defclass Woman
          (extends Person)
          (method greet [v] (str "Hello, " v))
          (method age [] (* 2 9)))
;=> #'ch08.core/Woman

; now this is fixed
(def donna (Woman :new))
;=> #'ch08.core/donna
(donna :greet "Shelly")
;=> "Hello, Shelly"
(donna :age)
;=> 18
(donna :about 3)
;=> "I was born about 21 years ago"

;; A simple object system for Clojure
(declare ^:dynamic this)
(declare find-method)
(defn new-object [klass]
  (let [state (ref {})]
    (fn thiz [command & args]
      (case command
        :class klass
        :class-name (klass :name)
        :set! (let [[k v] args]
                (dosync (alter state assoc k v))
                nil)
        :get (let [[key] args]
               (@state key))
        (let [method (klass :method command)]
          (if-not method
            (throw (RuntimeException.
                     (str "Unable to respond to " command))))
          (binding [this thiz]
            (apply method args)))))))
(defn new-class [class-name parent methods]
  (fn klass [command & args]
    (case command
      :name (name class-name)
      :parent parent
      :new (new-object klass)
      :methods methods
      :method (let [[method-name] args]
                (find-method method-name klass)))))
(def OBJECT (new-class :OBJECT nil {}))
(defn find-method [method-name klass]
  (or ((klass :methods) method-name)
      (if-not (= #'OBJECT klass)
        (find-method method-name (klass :parent)))))
(defn method-spec [sexpr]
  (let [name (keyword (second sexpr))
        body (next sexpr)]
    [name (conj body 'fn)]))
(defn method-specs [sexprs]
  (->> sexprs
       (filter #(= 'method (first %)))
       (mapcat method-spec)
       (apply hash-map)))
(defn parent-class-spec [sexprs]
  (let [extends-spec (filter #(= 'extends (first %)) sexprs)
        extends (first extends-spec)]
    (if (empty? extends)
      'OBJECT
      (last extends))))
(defmacro defclass [class-name & specs]
  (let [parent-class (parent-class-spec specs)
        fns (or (method-specs specs) {})]
    `(def ~class-name (new-class '~class-name  #'~parent-class ~fns))))
