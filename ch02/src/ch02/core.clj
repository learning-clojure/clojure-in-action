(ns ch02.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

;; Clojure Repl
(+ 1 2)

(def my-addition (fn [opernad1 operand2] (+ opernad1 operand2)))
(my-addition 1 2)
(my-addition 100 30)

(+ 1 2) "Two forms on one line!"

;; “Hello, world!”
(println "Hello, world!")

;; MAGIC REPL VARIABLES
"expression 1"
;=> "expression 1"
"expression 2"
;=> "expression 2"
*1
;=> "expression 2"
*3
;=> "expression 1"
"a" "b" "c"
;=> "a"
;=> "b"
;=> "c"
*3
;=> "a"
(def a-str *1)
;=> #'ch02/a
a-str
;=> "something else"
"something else"
;=> "something else"
a-str
;=> "a"
))
;=> ()
; RuntimeException Unmatched delimiter: ) clojure.lang.Util.runtimeException (Util.java:221)
*1
;=> "a"
*e
; #error{:cause "Unmatched delimiter: )",

;; DOC
(doc +)
(+)
;=> 0
(+ 1)
;=> 1
(+ 1 2)
;=> 3
(+ 1 2 3)
;=> 6
(+ 1 2 3 4 5 6 7 8)
;=> 36

;; CLOJURE SYNTAX: PREFIX NOTATION

(def x 1)
; => #'ch02.core/x
(cond
  (> x 0) "greater!"
  (= x 0) "zero!"
  (< x 0) "lesser!")
; => "greater!"

;; WHITESPACES

(+ 1 2 3 4 5)
;=> 15
(+ 1, 2, 3, 4, 5)
;=> 15
(+ 1,2,3,4,5)
;=> 15
(+ 1,,,,,2,3 4,,5)
;=> 15

(def a-map {:a 1 :b 2 :c 3})
; => #'ch02.core/a-map

a-map
; {:a 1, :c 3, :b 2}

;; Clojure data structures

;; Characters and strings
(.contains "clojure-in-action" "-")
; => true

(.endsWith "program.clj" ".clj")
; => true

;; Clojure numbers

(+ 1 1N)
;=> 2N
(+ 1 1N 1/2)
;=> 5/2
(+ 1 1N 1/2 0.5M)
;=> 3.0M
(+ 1 1N 1/2 0.5M 0.5)
;=> 3.5

(inc 9223372036854775807)
; ArithmeticException integer overflow  clojure.lang.Numbers.throwIntOverflow (Numbers.java:1501)

(inc' 9223372036854775807)
; => 9223372036854775808N

;; Symbols and keywords
arglebarg
; CompilerException java.lang.RuntimeException: Unable to resolve symbol: arglebarg in this context, compiling:(/private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init8200590746876817644.clj:1:925)
'arglebarg
;=> arglebarg

(keyword "foo")
;=> :foo
(symbol "foo" "bar")
;=> foo/bar
(name :foo/bar)
;=> "bar"
(namespace :foo)
;=> nil
(name "baz")
;=> "baz"

;; Lists
(list 1 2 3 4 5)
;=> (1 2 3 4 5)
(list? *1)
;=> true

(conj (list 1 2 3 4 5) 6)
;=> (6 1 2 3 4 5)
(conj (list 1 2 3) 4 5 6)
;=> (6 5 4 1 2 3)
(conj (conj (conj (list 1 2 3) 4) 5) 6)
;=> (6 5 4 1 2 3)

(peek (list 1 2 3))
;=> 1
(pop (list 1 2 3))
;=> (2 3)
(peek (list))
;=> nil
(pop (list))
; IllegalStateException Can't pop empty list  clojure.lang.PersistentList$EmptyList.pop (PersistentList.java:209)

(count (list))
;=> 0
(count (list 1 2 3 4))
;=> 4

(def three-numbers (1 2 3))
; CompilerException java.lang.ClassCastException: java.lang.Long cannot be cast to clojure.lang.IFn, compiling:(form-init8200590746876817644.clj:1:20)

(def three-numbers '(1 2 3))
;=> #'ch02.core/three-numbers

;; Vectors

(vector 10 20 30 40 50)
;=> [10 20 30 40 50]
(def the-vector [10 20 30 40 50])
; => #'ch02.core/the-vector

(get the-vector 2)
;=> 30
(nth the-vector 2)
;=> 30
(get the-vector 10)
;=> nil

(nth the-vector 10)
; IndexOutOfBoundsException   clojure.lang.PersistentVector.arrayFor (PersistentVector.java:158)

(assoc the-vector 2 25)
;=> [10 20 25 40 50]
(assoc the-vector 5 60)
;=> [10 20 30 40 50 60]
(assoc the-vector 6 70)
; IndexOutOfBoundsException   clojure.lang.PersistentVector.assocN (PersistentVector.java:188)

(conj [1 2 3 4 5] 6)
;=> [1 2 3 4 5 6]

(peek [1 2])
;=> 2
(pop [1 2])
;=> [1]
(peek [])
;=> nil
(pop [])
; IllegalStateException Can't pop empty vector  clojure.lang.PersistentVector.pop (PersistentVector.java:468)

(the-vector 3)
;=> 40

;; Maps
(def the-map {:a 1 :b 2 :c 3})
; => #'ch02.core/the-map

(hash-map :a 1 :b 2 :c 3)
; => {:c 3, :b 2, :a 1}

(the-map :b)
;=> 2
(:b the-map)
;=> 2
(:z the-map 26)
;=> 26

(def updated-map (assoc the-map :d 4))
;=> #'ch02.core/updated-map
updated-map
;=> {:d 4, :a 1, :b 2, :c 3}
(dissoc updated-map :a)
;=> {:b 2, :c 3, :d 4}

(def users {:kyle {
                   :date-joined "2009-01-01"
                   :summary {
                             :average {
                                       :monthly 1000
                                       :yearly 12000}}}})
;=> #'ch02.core/users
(assoc-in users [:kyle :summary :average :monthly] 3000)
; => {:kyle {:date-joined "2009-01-01", :summary {:average {:monthly 3000, :yearly 12000}}}}
; (assoc-in map [key & more-keys] value)

(get-in users [:kyle :summary :average :monthly])
;=> 1000

(update-in users [:kyle :summary :average :monthly] + 500)
;=> {:kyle {:date-joined "2009-01-01", :summary {:average {:monthly 1500, :yearly 12000}}}}
; (update-in map [key & more-keys] update-function & args)

;; Sequences
(first (list 1 2 3))
;=> 1
(rest (list 1 2 3))
;=> (2 3)
(first [1 2 3])
;=> 1
(rest [1 2 3])
;=> (2 3)
(first {:a 1 :b 2})
;=> [:a 1] ; but order of items isn't guaranteed
(rest {:a 1 :b 2})
;=> ([:b 2])
(first [])
;=> nil
(rest [])
;=> ()

(cons 1 [2 3 4 5])
;=> (1 2 3 4 5)

(list? (cons 1 (list 2 3)))
;=> false

;; Program structure
;; Functions

(defn addition-function [x y]
  (+ x y))
; => #'ch02.core/addition-function
(addition-function 1 2)
; => 3

;; The let form
(let [x 1
      y 2
      z (+ x y)] z)
; => 3

;; UNDERSCORE IDENTIFIER
(defn average-pets []
  (let [user-data (vals users)
        pet-counts (map :number-pets user-data)
        value-from-println (println "total pets:" pet-counts)
        total (apply + pet-counts)]
    (/ total (count users))))

; => #'ch02.core/average-pets

;; same but without unused variable which is replaced with underscore _
(defn average-pets []
  (let [user-data (vals users)
        pet-counts (map :number-pets user-data)
        _ (println "total pets:" pet-counts)
        total (apply + pet-counts)]
    (/ total (count users))))

; => #'ch02.core/average-pets

;; Program flow
;; Conditionals
;; IF
(if (> 5 2)
  "yes"
  "no")
;=> "yes"

;; IF-NOT
(if-not (> 5 2) "yes" "no")
;=> "no"

;; COND
(def x 1)
;=> #'user/x
(cond
  (> x 0)  "greater!"
  (= x 0)  "zero!"
  :default "lesser!")
;=> "greater!"

;; WHEN
(when (> 5 2)
  (println "five")
  (println "is")
  (println "greater")
  "done")
; five
; is
; greater
;=> "done"

;; WHEN-NOT
(when-not (< 5 2)
  (println "two")
  (println "is")
  (println "smaller")
  "done")
; two
; is
; smaller
; => "done"

;; Logical functions
(and)
;=> true
(and :a :b :c)
;=> :c
(and :a nil :c)
;=> nil
(and :a false :c)
;=> false
(and 0 "")
;=> ""

(or)
;=> nil
(or :a :b :c)
;=> :a
(or :a nil :c)
;=> :a
(or nil false)
;=> false
(or false nil)
;=> nil

(not true)
;=> false
(not 1)
;=> false
(not nil)
;=> true

(< 2 4 6 8)
;=> true
(< 2 4 3 8)
;=> false

;; = (SINGLE-EQUALS) VS. == (DOUBLE-EQUALS)
(= 1 1N 1/1)
;=> true
(= 0.5 1/2)
;=> false
(= 0.5M 0.5)
;=> false
(= 0.5M 1/2)
;=> false

(== 1 1N 1/1)
;=> true
(== 1/2 0.5M 0.5)
;=> true
1.9999999999999999
;=> 2.0
(== 2.0M 1.9999999999999999)
;=> true
(== :a 1)
; ClassCastException clojure.lang.Keyword cannot be cast to java.lang.Number  clojure.lang.Numbers.equiv (Numbers.java:208)

;; Functional iteration
;; WHILE
;(while (request-on-queue?)
;  (handle-request (pop-request-queue)))

;; LOOP/RECUR
(defn fact-loop [n]
  (loop [current n fact 1]
    (if (= current 1)
      fact
      (recur (dec current) (* fact current)))))

;=> #'ch02.core/fact-loop

(fact-loop 5)
;=> 120

;; DOSEQ AND DOTIMES
(defn run-report [user]
  (println "Running report for" user))

(defn dispatch-reporting-jobs [all-users]
  (doseq [user all-users]
    (run-report user)))


(dotimes [x 5]
  (println "X is" x))
;X is 0
;X is 1
;X is 2
;X is 3
;X is 4
;=> nil

;; MAP
(map inc [0 1 2 3])
;=> (1 2 3 4)

(map + [0 1 2 3] [0 1 2 3])
;=> (0 2 4 6)
(map + [0 1 2 3] [0 1 2])
;=> (0 2 4)

;; FILTER AND REMOVE
(defn non-zero-expenses [expenses]
  (let [non-zero? (fn [e] (not (zero? e)))]
    (filter non-zero? expenses)))
;=> #'ch02.core/non-zero-expenses
(non-zero-expenses [-2 -1 0 1 2 3])
;=> (-2 -1 1 2 3)

(defn non-zero-expenses [expenses]
  (remove zero? expenses))
;=> #'ch02.core/non-zero-expenses
(non-zero-expenses [-2 -1 0 1 2 3])
;=> (-2 -1 1 2 3)

;; REDUCE
(defn factorial [n]
  (let [numbers (range 1 (+ n 1))]
    (reduce * numbers)))
;=> #'ch02.core/factorial

(range 10)
;=> (0 1 2 3 4 5 6 7 8 9)

(factorial 5)
;=> 120

(defn factorial-steps [n]
  (let [numbers (range 1 (+ n 1))]
    (reductions * numbers)))
;=> #'ch02.core/factorial-steps

(factorial-steps 5)
;=> (1 2 6 24 120)
(factorial 1)
;=> 1
(factorial 2)
;=> 2
(factorial 3)
;=> 6
(factorial 4)
;=> 24
(factorial 5)
;=> 120 #A
(map factorial (range 1 6))
;=> (1 2 6 24 120)

;; FOR
; (for seq-exprs body-expr)
(def chessboard-labels
  (for [alpha "abcdefgh"
        num (range 1 9)]
    (str alpha num)))
;=> #'ch02.core/chessboard-labels
chessboard-labels
;=>
;("a1"
;  "a2"
;  "a3"
;  ...
;  "h6"
;  "h7"
;  "h8")

(defn prime? [x]
  (let [divisors (range 2 (inc (int (Math/sqrt x))))
        remainders (map (fn [d] (rem x d)) divisors)]
    (not (some zero? remainders))))
;=> #'ch02.core/prime?
(prime? 3)
;=> true
(prime? 4)
;=> false


(defn primes-less-than [n]
  (for [x (range 2 (inc n))
        :when (prime? x)]
    x))
;=> #'ch02.core/primes-less-than
(primes-less-than 50)
;=> (2 3 5 7 11 13 17 19 23 29 31 37 41 43 47)

(defn pairs-for-primes [n] (let [z (range 2 (inc n))]
                             (for [x z y z :when (prime? (+ x y))]
                               (list x y))))
;=> #'ch02.core/pairs-for-primes
(pairs-for-primes 5)
;=> ((2 3) (2 5) (3 2) (3 4) (4 3) (5 2))

;; Threading macros
;; THREAD-FIRST
(defn final-amount [principle rate time-periods]
  (* (Math/pow (+ 1 (/ rate 100)) time-periods) principle))
;=> #'ch02.core/final-amount
(final-amount 100 20 1)
;=> 120.0
(final-amount 100 20 2)
;=> 144.0

;; same with hread-first macro (->)
(defn final-amount-> [principle rate time-periods]
  (-> rate
      (/ 100)
      (+ 1)
      (Math/pow time-periods)
      (* principle)))
;=> #'ch02.core/final-amount->
(final-amount-> 100 20 1)
;=> 120.0
(final-amount-> 100 20 2)
;=> 144.0

;; THREAD-LAST

(defn factorial [n]
  (reduce * (range 1 (+ 1 n))))
;=> #'ch02.core/factorial

;; same function rewritten using the ->> macro
(defn factorial->> [n]
  (->> n
       (+ 1)
       (range 1)
       (reduce *)))
;=> #'ch02.core/factorial->>
(factorial->> 5)
;=> 120

;; THREAD-AS
(as-> {"a" [1 2 3 4]} <>
      (<> "a")
      (conj <> 10)
      (map inc <>))
;=> (2 3 4 5 11)
;; it's same as this
(let [<> {"a" [1 2 3 4]}
      <> (<> "a")
      <> (conj <> 10)
      <> (map inc <>)]
  <>)

;; CONDITIONAL THREADING
(let [x 1 y 2]
  (cond-> []
          (odd? x) (conj "x is odd")
          (zero? (rem y 3)) (conj "y is devisible by 3")
          (even? y) (conj "y is even")))
;=> ["x is odd" "y is even"]

; same as following:

(let [x 1 y 2]
  (as-> [] <>
        (if (odd? x)          (conj <> "x is odd")            <>)
        (if (zero? (rem y 3)) (conj <> "y is divisible by 3") <>)
        (if (even? y)         (conj <> "y is even")           <>)))
;=> ["x is odd" "y is even"]

