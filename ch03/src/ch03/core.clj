(ns ch03.core
  (:gen-class))

;; 3.1 Metadata
(def untrusted (with-meta {:command "delete-table" :subject "users"}
                          {:safe false :io true}))
;=> #'ch03.core/untrusted
(meta untrusted)
;=> {:safe false, :io true}

; same with shortcat
(def second-untrusted ^{:safe false :io true} {:command "delete-table"
                                               :subject "users"})
;=> #'ch03.core/second-untrusted
(meta second-untrusted)
;=> {:safe false, :io true}

(def untrusted-with-hidden-meta ^{:safe false :io true} (hash-map :command "delete-table"
                                                                  :subject "users"))
;=> #'ch03.core/untrusted-with-hidden-meta
(meta untrusted-with-hidden-meta)
;=> nil
;This associates metadata with the list starting with hash-map, not the hash map, so this
; metadata becomes invisible at runtime.

;; comparing similar objects but without meta
untrusted
;=> {:command "delete-table", :subject "users"}
(def trusted {:command "delete-table" :subject "users"})
;=> #'ch03.core/trusted
(= trusted untrusted)
;=> true
(meta untrusted)
;=> {:safe false, :io true}
(meta trusted)
;=> nil

;; When new values are created from those that have metadata, the metadata is copied over
;; to the new data.
(def still-untrusted (assoc untrusted :complete? false))
;=> #'ch03.core/still-untrusted
still-untrusted
;=> {:command "delete-table", :subject "users", :complete? false}
(meta still-untrusted)
;=> {:safe false, :io true}

;; Functions and macros can also be defined with metadata.
(defn ^{:safe true :console true
        :doc  "testing metadata for functions"}
testing-meta
  []
  (println "Hello from meta!"))
;=> #'ch03.core/testing-meta

;; BUT
(meta testing-meta)
;=> nil

;; to get meta from function
(meta (var testing-meta))
;=>
;{:safe true,
; :console true,
; :doc "testing metadata for functions",
; :arglists ([]),
; :line 1,
; :column 1,
; :file "/private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init4121243272943126527.clj",
; :name testing-meta,
; :ns #object[clojure.lang.Namespace 0x46efa30f "ch03.core"]}

;; 3.1.1 Java type hints

(set! *warn-on-reflection* true)                            ; Warns you when reflection is needed
;=> true
(defn string-length [x] (.length x))
;Reflection warning, /private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init4121243272943126527.clj:1:25 - reference to field length can't be resolved.
;=> #'ch03.core/string-length
(time (reduce + (map string-length (repeat 10000 "12345"))))
;"Elapsed time: 293.089463 msecs"
;=> 50000
(defn fast-string-length [^String x] (.length x))           ; No reflection warning
;=> #'ch03.core/fast-string-length
(time (reduce + (map fast-string-length (repeat 10000 "12345"))))
;"Elapsed time: 21.357529 msecs"
;=> 50000
(meta #'fast-string-length)
;=>
;{:arglists ([x]),
; :line 1,
; :column 1,
; :file "/private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init4121243272943126527.clj",
; :name fast-string-length,
; :ns #object[clojure.lang.Namespace 0x46efa30f "ch03.core"]}
(meta (first (first (:arglists (meta #'fast-string-length))))) ; Type hint on the function argument
;=> {:tag String}

;; 3.1.2 Java primitive and array types

; class returns an instance of java.lang.Class representing the class of its argument;
; Class has a method getName that returns class name as a string.
(defn array-type [klass]
  (.getName (class (make-array klass 0))))
;=> #'ch03.core/array-type
(array-type BigDecimal)
;=> "[Ljava.math.BigDecimal;"
(def bigdec-arr
  ^"[Ljava.math.BigDecimal;"                                ; Type hint with a string name
  (into-array BigDecimal [1.0M]))
;=> #'ch03.core/bigdec-arr
; into-array returns a Java array of a specified type filled with items from a Clojure collection.

;; 3.2 Java exceptions: try and throw
(defn average [numbers]
  (let [total (apply + numbers)]
    (/ total (count numbers))))
;=> #'ch03.core/average
(average [])
;ArithmeticException Divide by zero  clojure.lang.Numbers.divide (Numbers.java:158)

(defn safe-average [numbers]
  (let [total (apply + numbers)]
    (try
      (/ total (count numbers))
      (catch ArithmeticException e
        (println "Divided by zero!")
        0))))
;=> #'ch03.core/safe-average
(safe-average [])
;Divided by zero!
;=> 0

(try
  (print "Attempting division... ")
  (/ 1 0)
  (catch RuntimeException e "Runtime exception!")
  (catch ArithmeticException e "DIVIDE BY ZERO!")
  (catch Throwable e "Unknown exception encountered!")
  (finally
    (println "done.")))
;Attempting division... done.
;=> "Runtime exception!"
(try
  (print "Attempting division... ")
  (/ 1 0)
  (finally
    (println "done.")))
;Attempting division... done.
;ArithmeticException Divide by zero  clojure.lang.Numbers.divide (Numbers.java:158)

(throw (Exception. "this is an error!"))
;Exception this is an error!  ch03.core/eval1516 (form-init4121243272943126527.clj:1)

;; 3.3 Functions

;; 3.3.1 Defining functions

(defn total-cost [item-cost number-of-items]
  (* item-cost number-of-items))
;=> #'ch03.core/total-cost

(def total-cost (fn [item-cost number-of-items]
                  (* item-cost number-of-items)))
;=> #'ch03.core/total-cost

(defn total-cost
  "return line-item total of the item and quantity provided"
  [item-cost number-of-items]
  (* item-cost number-of-items))
;=> #'ch03.core/total-cost

(meta #'total-cost)
;=>
;{:arglists ([item-cost number-of-items]),
; :doc "return line-item total of the item and quantity provided",
; :line 1,
; :column 1,
; :file "/private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init3181494238074122279.clj",
; :name total-cost,
; :ns #object[clojure.lang.Namespace 0x7c15df38 "ch03.core"]}

(doc total-cost)
;-------------------------
;ch03.core/total-cost
;([item-cost number-of-items])
;return line-item total of the item and quantity provided
;=> nil

(meta (defn myfn-attr-map {:a 1} []))                       ; Metadata without using ^{}
;=>
;{:arglists ([]),
; :a 1,
; :line 1,
; :column 7,
; :file "/private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init3181494238074122279.clj",
; :name myfn-attr-map,
; :ns #object[clojure.lang.Namespace 0x7c15df38 "ch03.core"]}

(meta (defn ^{:a 1} myfn-metadata []))                      ; Notice ^{} before name
;=>
;{:a 1,
; :arglists ([]),
; :line 1,
; :column 7,
; :file "/private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init3181494238074122279.clj",
; :name myfn-metadata,
; :ns #object[clojure.lang.Namespace 0x7c15df38 "ch03.core"]}

(meta (defn ^{:a 1} myfn-both {:a 2 :b 3} []))              ; Metadata merged together
;=>
;{:a 2,
; :arglists ([]),
; :b 3,
; :line 1,
; :column 7,
; :file "/private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init3181494238074122279.clj",
; :name myfn-both,
; :ns #object[clojure.lang.Namespace 0x7c15df38 "ch03.core"]}

(meta (defn ^{:a 1 :doc "doc 1"} myfn-redundant-docs "doc 2" {:a 2 :b 3 :doc "doc 3"} [])) ; Metadata key-value pairs from different metadata sources overwrite each other from left to right.
;=>
;{:a 2,
; :doc "doc 3",
; :arglists ([]),
; :b 3,
; :line 1,
; :column 7,
; :file "/private/var/folders/mc/7k2x1sqn26b6cnvh7p0lzddh0000gn/T/form-init3181494238074122279.clj",
; :name myfn-redundant-docs,
; :ns #object[clojure.lang.Namespace 0x7c15df38 "ch03.core"]}

(defn item-total [price quantity discount-percentage]
  {:pre  [(> price 0) (> quantity 0)]
   :post [(> % 0)]}
  (->> (/ discount-percentage 100)
       (- 1)
       (* price quantity)
       float))
;=> #'ch03.core/item-total

(item-total 100 2 0)
;=> 200.0
(item-total 100 2 10)
;=> 180.0
(item-total 100 -2 10)
;AssertionError Assert failed: (> quantity 0)  ch03.core/item-total (form-init3181494238074122279.clj:1)
(item-total 100 2 110)
;AssertionError Assert failed: (> % 0)  ch03.core/item-total (form-init3181494238074122279.clj:1)

;; MULTIPLE ARITY

(defn total-cost
  ([item-cost number-of-items]
   (* item-cost number-of-items))
  ([item-cost]
   (total-cost item-cost 1)))
;=> #'ch03.core/total-cost

;; VARIADIC FUNCTIONS

(defn total-all-numbers [& numbers]
  (apply + numbers))
;=> #'ch03.core/total-all-numbers

(defn many-arities
  ([]             0)
  ([a]            1)
  ([a b c]        3)
  ([a b c & more] "variadic"))
;=> #'user/many-arities
(many-arities)
;=> 0
(many-arities "one argument")
;=> 1
;(many-arities "two" "arguments")
; ArityException Wrong number of args (2) passed to: core/many-arities  clojure.lang.AFn.throwArity (AFn.java:429)
(many-arities "three" "argu-" "ments")
;=> 3
(many-arities "many" "more" "argu-" "ments")
;=> "variadic"

;; RECURSIVE FUNCTIONS

(defn count-down [n]
  (when-not (zero? n)
    (when (zero? (rem n 100))                               ; rem means remainder of division.
      (println "count-down:" n))
    (count-down (dec n))))                                  ; dec means decrement by one
;=> #'ch03.core/count-down

(count-down 100000)
;count-down: 100000
;count-down: 99900
;...
;count-down: 94800
;StackOverflowError   clojure.tools.nrepl.middleware.session/session-out/fn--777 (session.clj:33)
;count-down:

(defn count-downr [n]
  (when-not (zero? n)
    (if (zero? (rem n 100))
      (println "count-down:" n))
    (recur (dec n))))
;=> #'ch03.core/count-downr

;; MUTUALLY RECURSIVE FUNCTIONS

;; Mutually recursive functions that can blow the stack
(declare hat)
(defn cat [n]
  (when-not (zero? n)
    (when (zero? (rem n 100))
      (println "cat:" n))
    (hat (dec n))))
(defn hat [n]
  (when-not (zero? n)
    (if (zero? (rem n 100))
      (println "hat:" n))
    (cat (dec n))))

;; Mutually recursive functions that can be called with trampoline
(declare hatt)
(defn catt [n]
  (when-not (zero? n)
    (when (zero? (rem n 100))
      (println "catt:" n)) (fn [] (hatt (dec n)))))
(defn hatt [n]
  (when-not (zero? n)
    (when (zero? (rem n 100))
      (println "hatt:" n))
    (fn [] (catt (dec n)))))

(trampoline catt 100000)
;catt: 100000
;catt: 99900
;catt: 99800
;catt: 300
;catt: 200
;catt: 100
;=> nil

;; 3.3.2 Calling functions

(+ 1 2 3 4 5)
;=> 15
(def list-of-expenses [1 2 3 4 5])
;=> #'ch03.core/list-of-expenses
(apply + list-of-expenses)
;=> 15

;; 3.3.3 Higher-order functions

;; EVER Y?
(def bools [true true true false false])
;=> #'ch03.core/bools
(every? true? bools)
;=> false

;; SOME
(some (fn [p] (= "rob" p)) ["kyle" "siva" "rob" "celeste"])
;=> true

;; CONSTANTLY
(def two (constantly 2)) ; same as (def two (fn [& more] 2))
                         ; or (defn two [& more] 2)
;=> #'ch03.core/two
(two 1)
;=> 2
(two :a :b :c)
;=> 2

;; COMPLEMENT
(defn greater? [x y]
  (> x y))
;=> #'ch03.core/greater?
(greater? 10 5)
;=> true
(greater? 10 20)
;=> false

(def smaller? (complement greater?))
;=> #'ch03.core/smaller?
(smaller? 10 5)
;=> false
(smaller? 10 20)
;=> true

;; COMP
(def opp-zero-str (comp str not zero?))
;=> #'ch03.core/opp-zero-str
(opp-zero-str 0)
;=> "false"
(opp-zero-str 1)
;=> "true"

;; PARTIAL
(defn above-threshold? [threshold number]
  (> number threshold))
;=> #'ch03.core/above-threshold?
(filter (fn [x] (above-threshold? 5 x)) [ 1 2 3 4 5 6 7 8 9])
;=> (6 7 8 9)
(filter (partial above-threshold? 5) [ 1 2 3 4 5 6 7 8 9])
;=> (6 7 8 9)

;; MEMOIZE
(defn slow-calc [n m]
  (Thread/sleep 1000)
  (* n m))
;=> #'ch03.core/slow-calc
(time (slow-calc 5 7))
;"Elapsed time: 1003.368246 msecs"
;=> 35

(def fast-calc (memoize slow-calc))
;=> #'ch03.core/fast-calc
(time (fast-calc 5 7))
;"Elapsed time: 1003.157716 msecs"
;=> 35
(time (fast-calc 5 7))
;"Elapsed time: 0.09232 msecs"
;=> 35

;; 3.3.4 Writing higher-order functions

;; Function composition using higher-order functions
(def users
  [{:username     "kyle"
    :firstname    "Kyle"
    :lastname     "Smith"
    :balance      175.00M             ; Use BigDecimals for money!
    :member-since "2009-04-16"}
   {:username     "zak"
    :firstname    "Zackary"
    :lastname     "Jones"
    :balance      12.95M
    :member-since "2009-02-01"}
   {:username     "rob"
    :firstname    "Robert"
    :lastname     "Jones"
    :balance      98.50M
    :member-since "2009-03-30"}])
;=> #'ch03.core/users
(defn sorter-using [ordering-fn]
  (fn [collection]
    (sort-by ordering-fn collection)))
;=> #'ch03.core/sorter-using
(defn lastname-firstname [user]
  [(user :lastname) (user :firstname)])
;=> #'ch03.core/lastname-firstname
(defn balance [user] (user :balance))
;=> #'ch03.core/balance
(defn username [user] (user :username))
;=> #'ch03.core/username
(def poorest-first (sorter-using balance))
;=> #'ch03.core/poorest-first
(def alphabetically (sorter-using username))
;=> #'ch03.core/alphabetically
(def last-then-firstname (sorter-using lastname-firstname))
;=> #'ch03.core/last-then-firstname

(map username users)
;=> ("kyle" "zak" "rob")
(sort *1)                                                   ; *1 means “last result returned by REPL.”
;=> ("kyle" "rob" "zak")

(sort-by username users)
;=>
;({:username "kyle",
;  :firstname "Kyle",
;  :lastname "Smith",
;  :balance 175.00M,
;  :member-since "2009-04-16"}
;  {:username "rob",
;   :firstname "Robert",
;   :lastname "Jones",
;   :balance 98.50M,
;   :member-since "2009-03-30"}
;  {:username "zak",
;   :firstname "Zackary",
;   :lastname "Jones",
;   :balance 12.95M,
;   :member-since "2009-02-01"})

(def poorest-first (sorter-using balance))
;=> #'ch03.core/poorest-first
; same as
; (defn poorest-first [users] (sort-by balance users))
(poorest-first users)
;=>
;({:username "zak",
;  :firstname "Zackary",
;  :lastname "Jones",
;  :balance 12.95M,
;  :member-since "2009-02-01"}
;  {:username "rob",
;   :firstname "Robert",
;   :lastname "Jones",
;   :balance 98.50M,
;   :member-since "2009-03-30"}
;  {:username "kyle",
;   :firstname "Kyle",
;   :lastname "Smith",
;   :balance 175.00M,
;   :member-since "2009-04-16"})

(map lastname-firstname users)
;=> (["Smith" "Kyle"] ["Jones" "Zackary"] ["Jones" "Robert"])
(sort *1)
;=> (["Jones" "Robert"] ["Jones" "Zackary"] ["Smith" "Kyle"])

(last-then-firstname users)
;=>
;({:username "rob",
;  :firstname "Robert",
;  :lastname "Jones",
;  :balance 98.50M,
;  :member-since "2009-03-30"}
;  {:username "zak",
;   :firstname "Zackary",
;   :lastname "Jones",
;   :balance 12.95M,
;   :member-since "2009-02-01"}
;  {:username "kyle",
;   :firstname "Kyle",
;   :lastname "Smith",
;   :balance 175.00M,
;   :member-since "2009-04-16"})

;; 3.3.5 Anonymous functions

(map (fn [user] (user :member-since)) users)
;=> ("2009-04-16" "2009-02-01" "2009-03-30")

;; A SHORTCUT FOR ANONYMOUS FUNCTIONS
(map #(% :member-since) users)
;=> ("2009-04-16" "2009-02-01" "2009-03-30")

(#(vector %&) 1 2 3 4 5)
;=> [(1 2 3 4 5)]
(#(vector % %&) 1 2 3 4 5)
;=> [1 (2 3 4 5)]
(#(vector %1 %2 %&) 1 2 3 4 5)
;=> [1 2 (3 4 5)]
(#(vector %1 %2 %&) 1 2)
;=> [1 2 nil]

;; 3.3.6 Keywords and symbols

(def person {:username "zak"
             :balance 12.95
             :member-since "2009-02-01"})
;=> #'ch03.core/person

(person :username)
;=> "zak"
; same as
(:username person)
;=> "zak"

(map #(% :member-since) users)
;=> ("2009-04-16" "2009-02-01" "2009-03-30")
; same can be written shorter as following:
(map :member-since users)
;=> ("2009-04-16" "2009-02-01" "2009-03-30")

; optional parameter
(:login person)
;=> nil
(:login person :not-found)
;=> :not-found

;; SYMBOLS

(def expense {'name "Snow Leopard" 'cost 29.95M})
;=> #'user/expense
(expense 'name)
;=> "Snow Leopard"
('name expense)
;=> "Snow Leopard"
('vendor expense)
;=> nil
('vendor expense :absent)
;=> :absent

(person :username)
;=> "zak"
(person :login :not-found)
;=> :not-found
(def names ["kyle" "zak" "rob"])
;=> #'ch03.core/names
(names 1)
;=> "zak"

;; 3.4 Scope

;; 3.4.1 Vars and binding

; static var
(def MAX-CONNECTIONS 10)

; dynamic var

(def ^:dynamic RABBITMQ-CONNECTION)
(binding [RABBITMQ-CONNECTION (new-connection)]
  (
    ;; do something here with RABBITMQ-CONNECTION
    ))

;; dynamic scope
(def ^:dynamic *eval-me* 10)
(defn print-the-var [label]
  (println label *eval-me*))
(print-the-var "A:")
(binding [*eval-me* 20] ;; the first binding
  (print-the-var "B:")
  (binding [*eval-me* 30] ;; the second binding
    (print-the-var "C:"))
  (print-the-var "D:"))
(print-the-var "E:")
;=> #'ch03.core/*eval-me*
;=> #'ch03.core/print-the-var
;A: 10
;=> nil
;B: 20
;C: 30
;D: 20
;=> nil
;E: 10
;=> nil

; A higher-order function for aspect-oriented logging
(defn ^:dynamic twice [x]
  (println "original function")
  (* 2 x))

(defn call-twice [y]
  (twice y))

(defn with-log [function-to-call log-statement]
  (fn [& args]
    (println log-statement)
    (apply function-to-call args)))

(call-twice 10)

(binding [twice (with-log twice "Calling the twice function")]
  (call-twice 20))

(call-twice 30)

; result
;=> #'ch03.core/twice
;=> #'ch03.core/call-twice
;=> #'ch03.core/with-log
;original function
;=> 20
;Calling the twice function
;original function
;=> 40
;original function
;=> 60

;; LAZINESS AND SPECIAL VARIABLES
(def ^:dynamic *factor* 10)
(defn multiply [x]
  (* x *factor*))
;=> #'ch03.core/*factor*
;=> #'ch03.core/multiply
(map multiply [1 2 3 4 5])
;=> (10 20 30 40 50)
; new binding
(binding [*factor* 20]
  (map multiply [1 2 3 4 5]))
;=> (10 20 30 40 50)
; this doesn't work because sequences are lazy and aren't evaluated until they are used
; here's workaround(binding [*factor* 20]
(doall (map multiply [1 2 3 4 5])))
;=> (20 40 60 80 100)

;; 3.4.2 The let form revisited

(let [x 10
      y 20]
  (println "x, y:" x "," y))
;x, y: 10 , 20
;=> nil

(defn upcased-names [names]
  (let [up-case (fn [name] (.toUpperCase name))]
    (map up-case names)))
;=> #'ch03.core/upcased-names
(upcased-names ["foo" "bar" "baz"])
;=> ("FOO" "BAR" "BAZ")

; binding
(def ^:dynamic *factor* 10)
(binding [*factor* 20]
  (println *factor*)
  (doall (map multiply [1 2 3 4 5])))
;=> #'ch03.core/*factor*
;20
;=> (20 40 60 80 100)
; vs let
(let [*factor* 20]
  (println *factor*)
  (doall (map multiply [1 2 3 4 5])))
;20
;=> (10 20 30 40 50)

;; 3.4.3 Lexical closures

(defn create-scaler [scale]
  (fn [x]
    (* x scale)))
;=> #'ch03.core/create-scaler

; defining closure
(def percent-scaler (create-scaler 100))
;=> #'ch03.core/percent-scaler

(percent-scaler 0.59)
;=> 59.0

;; 3.5 Namespaces
;; 3.5.1 ns macro
(ns org.currylogic.damages.calculators)
(defn highest-expense-during [start-date end-date]
  ;; (logic to find the answer)
  )
;=> nil
;=> #'org.currylogic.damages.calculators/highest-expense-during

;; USE, REQUIRE
;; Using external libraries by calling use
(ns org.currylogic.damages.http.expenses)
(use 'clojure.data.json)
(use 'clojure.xml)
(declare load-totals)
(defn import-transactions-xml-from-bank [url]
  (let [xml-document (parse url)]
    ;; more code here
    ))
(defn totals-by-day [start-date end-date]
  (let [expenses-by-day (load-totals start-date end-date)]
    (json-str expenses-by-day)))

;; Using external libraries by calling require
(ns org.currylogic.damages.http.expenses)
(require '(clojure.data [json :as json-lib]))
(require '(clojure [xml :as xml-core]))
(declare load-totals)
(defn import-transactions-xml-from-bank [url]
  (let [xml-document (xml-core/parse url)]
    ;; more code here
    ))
(defn totals-by-day [start-date end-date]
  (let [expenses-by-day (load-totals start-date end-date)]
    (json-lib/json-str expenses-by-day)))

;; Using external libraries by calling require
(ns org.currylogic.damages.http.expenses
  (:require [clojure.data.json :as json-lib]
            [clojure.xml :as xml-core]))
(declare load-totals)
(defn import-transactions-xml-from-bank [url]
  (let [xml-document (xml-core/parse url)]
    ;; more code here
    ))
(defn totals-by-day [start-date end-date]
  (let [expenses-by-day (load-totals start-date end-date)]
    (json-lib/json-str expenses-by-day)))

;; RELOAD AND RELOAD-ALL

;(use 'org.currylogic.damages.http.expenses :reload)
;(require '(org.currylogic.damages.http [expenses :as exp]) :reload)

;; 3.6 Destructuring

(defn describe-salary [person]
  (let [first  (:first-name person)
        last   (:last-name  person)
        annual (:salary     person)]
    (println first last "earns" annual)))
;=> #'org.currylogic.damages.http.expenses/describe-salary

; same with destructuring

(defn describe-salary-2 [{first  :first-name
                          last   :last-name
                          annual :salary}]
  (println first last "earns" annual))
;=> #'org.currylogic.damages.http.expenses/describe-salary-2

;; 3.6.1 Vector bindings

(defn print-amounts [[amount-1 amount-2]]
  (println "amounts are:" amount-1 "and" amount-2))
(print-amounts [10.95 31.45])
;amounts are: 10.95 and 31.45
;=> nil

;; USING & AND :AS
(defn print-amounts-multiple [[amount-1 amount-2 & remaining]]
  (println "Amounts are:" amount-1 "," amount-2 "and" remaining))
;=> #'org.currylogic.damages.http.expenses/print-amounts-multiple
(print-amounts-multiple [10.95 31.45 22.36 2.95])
;Amounts are: 10.95 , 31.45 and (22.36 2.95)
;=> nil

(defn print-all-amounts [[amount-1 amount-2 & remaining :as all]]
  (println "Amounts are:" amount-1 "," amount-2 "and" remaining)
  (println "Also, all the amounts are:" all))
;=> #'org.currylogic.damages.http.expenses/print-all-amounts
(print-all-amounts [10.95 31.45 22.36 2.95])
;Amounts are: 10.95 , 31.45 and (22.36 2.95)
;Also, all the amounts are: [10.95 31.45 22.36 2.95]
;=> nil

;; NESTED VECTORS
(defn print-first-category [[[category amount] & _ ]]
  (println "First category was:" category)
  (println "First amount was:" amount))
;=> #'org.currylogic.damages.http.expenses/print-first-category
(def expenses [[:books 49.95] [:coffee 4.95] [:caltrain 2.25]])
(print-first-category expenses)
;=> #'org.currylogic.damages.http.expenses/expenses
;First category was: :books
;First amount was: 49.95
;=> nil

;; 3.6.2 Map bindings

(defn describe-salary-2 [{first  :first-name
                          last   :last-name
                          annual :salary}]
  (println first last "earns" annual))
;=> #'org.currylogic.damages.http.expenses/describe-salary-2

(defn describe-salary-3 [{first  :first-name
                          last   :last-name
                          annual :salary
                          bonus :bonus-percentage
                          :or {bonus 5}}]
  (println first last "earns" annual "with a" bonus "percent bonus"))
;=> #'org.currylogic.damages.http.expenses/describe-salary-3
(def a-user {:first-name        "pascal"
             :last-name         "dylan"
             :salary            85000
             :bonus-percentage  20})
(describe-salary-3 a-user)
;=> #'org.currylogic.damages.http.expenses/a-user
;pascal dylan earns 85000 with a 20 percent bonus
;=> nil

(def another-user {:first-name        "basic"
                   :last-name         "groovy"
                   :salary            70000})
(describe-salary-3 another-user)
;=> #'org.currylogic.damages.http.expenses/another-user
;basic groovy earns 70000 with a 5 percent bonus
;=> nil

(defn describe-person [{first  :first-name
                        last   :last-name
                        annual :salary
                        bonus :bonus-percentage
                        :or {bonus 5}
                        :as p}]
  (println "Info about" first last "is:" p)
  (println "Bonus is:" bonus "percent"))
;=> #'org.currylogic.damages.http.expenses/describe-person

(def third-user {:first-name        "lambda"
                   :last-name         "curry"
                   :salary            95000})
(describe-person third-user)
;=> #'org.currylogic.damages.http.expenses/third-user
;Info about lambda curry is: {:first-name lambda, :last-name curry, :salary 95000}
;Bonus is: 5 percent
;=> nil

(defn greet-user [{:keys [first-name last-name]}]
  (println "Welcome," first-name last-name))
;=> #'org.currylogic.damages.http.expenses/greet-user

(def roger {:first-name "roger" :last-name "mann" :salary 65000})
(greet-user roger)
;=> #'org.currylogic.damages.http.expenses/roger
;Welcome, roger mann
;=> nil

;; 3.7 Reader literals

(java.util.UUID/randomUUID)
;=> #uuid"257e28c8-31d7-40ca-901e-fb3bf3b67bc1"

(ns clj-in-act.ch3.reader
  (:import java.util.UUID))
(defn guid [four-letters-four-digits]
  (java.util.UUID/fromString
    (str four-letters-four-digits "-1000-413f-8a7a-f11c6a9c4036")))
;=> nil
;=> #'clj-in-act.ch3.reader/guid
(use 'clj-in-act.ch3.reader)
;=> nil
(guid "abcd1234")
;=> #uuid"abcd1234-1000-413f-8a7a-f11c6a9c4036"

; reader literals usage
#clj-in-act/G "abcd1234"
