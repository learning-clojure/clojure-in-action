(ns ch06.core
  (:gen-class))

;; 6 State and the concurrent world
;; 6.4 Refs
;; 6.4.1 Creating refs
(def all-users (ref {}))
;=> #'ch06.core/all-users
(deref all-users)
;=> {}
; or same with reader macro
@all-users
;=> {}
all-users
=> #object[clojure.lang.Ref 0x1212ac8d {:status :ready, :val {}}]

;; 6.4.2 Mutating refs
;; REF-SET
(ref-set all-users {})
;IllegalStateException No transaction running  clojure.lang.LockingTransaction.getEx (LockingTransaction.java:208)
; this happned because it run out of multiple thread scope
(dosync
  (ref-set all-users {}))
;=> {}

;; ALTER
(defn new-user [id login monthly-budget]
  {:id id
   :login login
   :monthly-budget monthly-budget
   :total-expenses 0})
;=> #'ch06.core/new-user

(defn add-new-user [login budget-amount]
  (dosync
    (let [current-number (count @all-users)
          user (new-user (inc current-number) login budget-amount)]
      (alter all-users assoc login user))))
;=> #'ch06.core/add-new-user

(add-new-user "amit" 1000000)
;=> {"amit" {:id 1, :login "amit", :monthly-budget 1000000, :total-expenses 0}}
(add-new-user "deepthi" 2000000)
;=>
;{"amit" {:id 1, :login "amit", :monthly-budget 1000000, :total-expenses 0},
; "deepthi" {:id 2, :login "deepthi", :monthly-budget 2000000, :total-expenses 0}}

;; 6.5 Agents
;; 6.5.1 Creating agents
(def total-cpu-time (agent 0))
;=> #'ch06.core/total-cpu-time

(deref total-cpu-time)
;=> 0

@total-cpu-time
;=> 0

;; 6.5.2 Mutating agents
;; SEND

(send total-cpu-time + 700)
;=> #object[clojure.lang.Agent 0x36507c7e {:status :ready, :val 700}]

(deref total-cpu-time)
;=> 700

;; 6.5.3 Working with agents
;; AWAIT AND AWAIT-FOR

;(await agent-one angent-two agent-three)

;(await-for 1000 agent-one agent-two agent-three)

;; AGENT ERRORS

(def bad-agent (agent 10))
;=> #'ch06.core/bad-agent
(send bad-agent / 0)
;=> #object[clojure.lang.Agent 0x7a56aa36 {:status :failed, :val 10}]
(deref bad-agent)
;=> 10
(send bad-agent / 2)
;ArithmeticException Divide by zero  clojure.lang.Numbers.divide (Numbers.java:158)
(agent-error bad-agent)
;=>
;#error{:cause "Divide by zero",
;       :via [{:type java.lang.ArithmeticException,
;              :message "Divide by zero",
;              :at [clojure.lang.Numbers divide "Numbers.java" 158]}],
;       :trace [[clojure.lang.Numbers divide "Numbers.java" 158]
;               [clojure.core$_SLASH_ invokeStatic "core.clj" 1008]
;               [clojure.core$_SLASH_ invoke "core.clj" 1001]
;               [clojure.core$binding_conveyor_fn$fn__4676 invoke "core.clj" 1944]
;               [clojure.lang.AFn applyToHelper "AFn.java" 156]
;               [clojure.lang.RestFn applyTo "RestFn.java" 132]
;               [clojure.lang.Agent$Action doRun "Agent.java" 114]
;               [clojure.lang.Agent$Action run "Agent.java" 163]
;               [java.util.concurrent.ThreadPoolExecutor runWorker "ThreadPoolExecutor.java" 1142]
;               [java.util.concurrent.ThreadPoolExecutor$Worker run "ThreadPoolExecutor.java" 617]
;               [java.lang.Thread run "Thread.java" 745]]}

(let [e (agent-error bad-agent)
      st (.getStackTrace e)]
  (println (.getMessage e))
  (println (clojure.string/join "\n" st)))
;Divide by zero
;clojure.lang.Numbers.divide(Numbers.java:158)
;clojure.core$_SLASH_.invokeStatic(core.clj:1008)
;clojure.core$_SLASH_.invoke(core.clj:1001)
;clojure.core$binding_conveyor_fn$fn__4676.invoke(core.clj:1944)
;clojure.lang.AFn.applyToHelper(AFn.java:156)
;clojure.lang.RestFn.applyTo(RestFn.java:132)
;clojure.lang.Agent$Action.doRun(Agent.java:114)
;clojure.lang.Agent$Action.run(Agent.java:163)
;java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
;java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
;java.lang.Thread.run(Thread.java:745)
;=> nil
(clear-agent-errors bad-agent)
;=> 10

;; 6.6 Atoms
;; 6.6.1 Creating atoms

(def total-rows (atom 0))
;=> #'ch06.core/total-rows
(deref total-rows)
;=> 0
@total-rows
;=> 0

;; 6.6.2 Mutating atoms

; (reset! atom new-value)

; (swap! the-atom the-function & more-args)
(swap! total-rows + 100)
;=> 100

; (compare-and-set! the-atom old-value new-value)

;; 6.7 Vars
;; 6.7.1 Creating vars and root bindings

(def hbase-master "localhost")
;=> #'ch06.core/hbase-master

(def ^:dynamic *hbase-master* "localhost")
;=> #'ch06.core/*hbase-master*
(println "Hbase-master is:" *hbase-master*)
;Hbase-master is: localhost
;=> nil

(def ^:dynamic *rabbitmq-host*)
;=> #'ch06.core/*rabbitmq-host*
(println "RabbitMQ host is:" *rabbitmq-host*)
;RabbitMQ host is: #object[clojure.lang.Var$Unbound 0xe4fa32f Unbound: #'ch06.core/*rabbitmq-host*]
;=> nil
(bound? #'*rabbitmq-host*)
;=> false

;; 6.7.2 Var bindings

(def ^:dynamic *mysql-host*)
;=> #'ch06.core/*mysql-host*

(defn db-query [db]
  (binding [*mysql-host* db]
    (count *mysql-host*)))
;=> #'ch06.core/db-query

(def mysql-hosts ["test-mysql" "dev-mysql" "staging-mysql"])
;=> #'ch06.core/mysql-hosts

(pmap db-query mysql-hosts)
;=> (10 9 13)

;; 6.8 State and its unified access model
;; 6.8.1 Creating
(def a-ref (ref 0))
(def an-agent (agent 0))
(def an-atom (atom 0))

;; 6.8.2 Reading
(deref a-ref)
;or
@a-ref
(deref an-agent)
;or
@an-agent
(deref an-atom)
;or
@an-atom

;; 6.8.3 Mutation
;; refs
;(ref-set ref new-value)
;(alter ref function & args)
;(commute ref function & args)

;; agents
;(send agent function & args)
;(send-off agent function & args)

;; atoms
;(reset! atom new-value)
;(swap! atom function & args)
;(compare-and-set! atom old-value new-value)

;; 6.8.5 Watching for mutation
;; ADD-WATCH
(def adi (atom 0))
;=> #'ch06.core/adi
(defn on-change [the-key the-ref old-value new-value]
  (println "Hey, seeing change from" old-value "to" new-value))
;=> #'ch06.core/on-change
(add-watch adi :adi-watcher on-change)
;=> #object[clojure.lang.Atom 0x8631e8f {:status :ready, :val 0}]
@adi
;=> 0
(swap! adi inc)
;Hey, seeing change from 0 to 1
;=> 1

;; REMOVE-WATCH
(remove-watch adi :adi-watcher)
;=> #object[clojure.lang.Atom 0x8631e8f {:status :ready, :val 1}]

;; 6.10 Futures and promises
;; 6.10.1 Futures

(defn long-calculation [num1 num2]
  (Thread/sleep 5000)
  (* num1 num2))
;=> #'ch06.core/long-calculation

(defn long-run []
  (let [x (long-calculation 11 13)
        y (long-calculation 13 17)
        z (long-calculation 17 19)]
    (* x y z)))
;=> #'ch06.core/long-run

(time (long-run))
;"Elapsed time: 15019.296466 msecs"
;=> 10207769

(defn fast-run []
  (let [x (future (long-calculation 11 13))
        y (future (long-calculation 13 17))
        z (future (long-calculation 17 19))]
    (* @x @y @z)))
;=> #'ch06.core/fast-run

(time (fast-run))
;"Elapsed time: 5004.731488 msecs"
;=> 10207769

;; 6.10.2 Promises

(def p (promise))

;(def value (deref p))
;or
;@p

(let [p (promise)]
  (future (Thread/sleep 5000)
          (deliver p :done))
  @p)
;=> :done
