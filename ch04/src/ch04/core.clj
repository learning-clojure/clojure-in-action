(ns ch04.core
  (:gen-class))

;; 4 Multimethod polymorphism
;; 4.1 Polymorphism and its types
;; 4.1.1 Parametric polymorphism
;; 4.1.2 Ad hoc polymorphism

(defn ad-hoc-type-namer [thing]
  (condp = (type thing)
    java.lang.String              "string"
    clojure.lang.PersistentVector "vector"))
;=> #'ch04.core/ad-hoc-type-namer
(ad-hoc-type-namer [])
;=> "vector"
(ad-hoc-type-namer {})
;IllegalArgumentException No matching clause: class clojure.lang.PersistentArrayMap  ch04.core/ad-hoc-type-namer (form-init5810412037143273989.clj:2)

(def type-namer-implementations
  {java.lang.String              (fn [thing] "string")
   clojure.lang.PersistentVector (fn [thing] "vector")})
;=> #'ch04.core/type-namer-implementations
(defn open-ad-hoc-type-namer [thing]
  (let [dispatch-value (type thing)]
    (if-let [implementation
             (get type-namer-implementations dispatch-value)]
      (implementation thing)
      (throw (IllegalArgumentException.
               (str "No implementation found for " dispatch-value))))))
;=> #'ch04.core/open-ad-hoc-type-namer
(open-ad-hoc-type-namer "I'm a string")
;=> "string"
(open-ad-hoc-type-namer [])
;=> "vector"
(open-ad-hoc-type-namer {})
;IllegalArgumentException No implementation found for class clojure.lang.PersistentArrayMap  ch04.core/open-ad-hoc-type-namer (form-init5810412037143273989.clj:7)

(def type-namer-implementations
  (assoc type-namer-implementations
    clojure.lang.PersistentArrayMap (fn [thing] "map")))
;=> #'ch04.core/type-namer-implementations
(open-ad-hoc-type-namer {})
;=> "map"

;; 4.1.3 Subtype polymorphism

(defn map-type-namer [thing]
  (condp = (type thing)
    clojure.lang.PersistentArrayMap "map"
    clojure.lang.PersistentHashMap  "map"))
;=> #'ch04.core/map-type-namer
(map-type-namer (hash-map))
;=> "map"
(map-type-namer (array-map))
;=> "map"
(map-type-namer (sorted-map))
;IllegalArgumentException No matching clause: class clojure.lang.PersistentTreeMap  ch04.core/map-type-namer (form-init5810412037143273989.clj:2)

(defn subtyping-map-type-namer [thing]
  (cond
    (instance? clojure.lang.APersistentMap thing) "map"
    :else (throw (IllegalArgumentException.
                   (str "No implementation found for ") (type thing)))))
;=> #'ch04.core/subtyping-map-type-namer
(subtyping-map-type-namer (hash-map))
;=> "map"
(subtyping-map-type-namer (array-map))
;=> "map"
(subtyping-map-type-namer (sorted-map))
;=> "map"

;; 4.2 Polymorphism using multimethods
;; 4.2.1 Life without multimethods

(def example-user {:login "rob" :referrer "mint.com" :salary 100000})
;=> #'ch04.core/example-user
(defn fee-amount [percentage user]
  (with-precision 16 :rounding HALF_EVEN
                     (* 0.01M percentage (:salary user))))
;=> #'ch04.core/fee-amount
(defn affiliate-fee [user]
  (case (:referrer user)
    "google.com"    (fee-amount 0.01M user)
    "mint.com"      (fee-amount 0.03M user)
    (fee-amount 0.02M user)))
;=> #'ch04.core/affiliate-fee
(affiliate-fee example-user)
;=> 30.0000M

;; 4.2.2 Ad hoc polymorphism using multimethods

(defmulti affiliate-fee (fn [user] (:referrer user)))
;=> #'ch04.core/affiliate-fee
(defmethod affiliate-fee "mint.com" [user]
  (fee-amount 0.03M user))
;=> #object[clojure.lang.MultiFn 0xb94a1 "clojure.lang.MultiFn@b94a1"]
(defmethod affiliate-fee "google.com" [user]
  (fee-amount 0.01M user))
;=> #object[clojure.lang.MultiFn 0xb94a1 "clojure.lang.MultiFn@b94a1"]
(defmethod affiliate-fee :default [user]
  (fee-amount 0.02M user))
;=> #object[clojure.lang.MultiFn 0xb94a1 "clojure.lang.MultiFn@b94a1"]
(affiliate-fee example-user)
;=> 30.0000M

;; DEFMULTI
(defmulti affiliate-fee :referrer :default "*")
;=> nil
; defmulti returning nil means it wasn’t redefined
(ns-unmap 'ch04.core 'affiliate-fee)
;=> nil
(defmulti affiliate-fee :referrer :default "*")
;=> => #'ch04.core/affiliate-fee
(defmethod affiliate-fee "*" [user]
  (fee-amount 0.02M user))
;=> #object[clojure.lang.MultiFn 0x53074d6a "clojure.lang.MultiFn@53074d6a"]
(affiliate-fee example-user)
;=> 20.0000M
;; Default case; "mint.com" case is gone because of redefined affiliate-fee.

;; DEFMETHOD

;(defmethod my-multi :default [arg] "body")
;(defmethod my-many-arity-multi :default
;  ([] "no arguments")
;  ([x] "one argument")
;  ([x & etc] "many arguments"))

(defmethod affiliate-fee "mint.com" [user]
  (fee-amount 0.03M user))
;=> #object[clojure.lang.MultiFn 0x53074d6a "clojure.lang.MultiFn@53074d6a"]
(defmethod affiliate-fee "google.com" [user]
  (fee-amount 0.01M user))
;=> #object[clojure.lang.MultiFn 0x53074d6a "clojure.lang.MultiFn@53074d6a"]
(methods affiliate-fee)
;=>
;{"mint.com" #object[ch04.core$eval1540$fn__1541 0x2ba1f99a "ch04.core$eval1540$fn__1541@2ba1f99a"],
; "*" #object[ch04.core$eval1514$fn__1515 0x183d78b7 "ch04.core$eval1514$fn__1515@183d78b7"],
; "google.com" #object[ch04.core$eval1552$fn__1553 0x6ff285a9 "ch04.core$eval1552$fn__1553@6ff285a9"]}
(get-method affiliate-fee "mint.com")
;=> #object[ch04.core$eval1540$fn__1541 0x2ba1f99a "ch04.core$eval1540$fn__1541@2ba1f99a"]
(get (methods affiliate-fee) "example.org")
;=> nil
(get-method affiliate-fee "example.org")
;=> #object[ch04.core$eval1514$fn__1515 0x183d78b7 "ch04.core$eval1514$fn__1515@183d78b7"]
((get-method affiliate-fee "mint.com") example-user)
;=> 30.0000M

;; 4.2.3 Multiple dispatch

(def user-1 {:login "rob"     :referrer "mint.com"   :salary 100000
             :rating :rating/bronze})
;=> #'ch04.core/user-1
(def user-2 {:login "gordon"  :referrer "mint.com"   :salary 80000
             :rating :rating/silver})
;=> #'ch04.core/user-2
(def user-3 {:login "kyle"    :referrer "google.com" :salary 90000
             :rating :rating/gold})
;=> #'ch04.core/user-3
(def user-4 {:login "celeste" :referrer "yahoo.com"  :salary 70000
             :rating :rating/platinum})
;=> #'ch04.core/user-4

(defn fee-category [user]
  [(:referrer user) (:rating user)])
;=> #'ch04.core/fee-category
(map fee-category [user-1 user-2 user-3 user-4])
;=>
;(["mint.com" :rating/bronze]
;  ["mint.com" :rating/silver]
;  ["google.com" :rating/gold]
;  ["yahoo.com" :rating/platinum])

(defmulti profit-based-affiliate-fee fee-category)
;=> #'ch04.core/profit-based-affiliate-fee
(defmethod profit-based-affiliate-fee ["mint.com" :rating/bronze]
  [user] (fee-amount 0.03M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(defmethod profit-based-affiliate-fee ["mint.com" :rating/silver]
  [user] (fee-amount 0.04M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(defmethod profit-based-affiliate-fee ["mint.com" :rating/gold]
  [user] (fee-amount 0.05M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(defmethod profit-based-affiliate-fee ["mint.com" :rating/platinum]
  [user] (fee-amount 0.05M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(defmethod profit-based-affiliate-fee ["google.com" :rating/gold]
  [user] (fee-amount 0.03M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(defmethod profit-based-affiliate-fee ["google.com" :rating/platinum]
  [user] (fee-amount 0.03M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(defmethod profit-based-affiliate-fee :default
  [user] (fee-amount 0.02M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(map profit-based-affiliate-fee [user-1 user-2 user-3 user-4])
;=> (30.0000M 32.0000M 27.0000M 14.0000M)

;; 4.2.4 Subtype polymorphism using multimethods
;; BUILDING AND QUERYING TYPE HIERARCHIES

(derive :rating/bronze :rating/basic)
;=> nil
(derive :rating/silver :rating/basic)
(derive :rating/gold :rating/premier)
(derive :rating/platinum :rating/premier)
(derive :rating/basic :rating/ANY)
(derive :rating/premier :rating/ANY)

(isa? :rating/gold :rating/premier)
;=> true
(isa? :rating/gold :rating/ANY)
;=> true
(isa? :rating/ANY :rating/premier)
;=> false
(isa? :rating/gold :rating/gold)
;=> true
(parents :rating/premier)
;=> #{:rating/ANY}
(ancestors :rating/gold)
;=> #{:rating/ANY :rating/premier}
(descendants :rating/ANY)
;=> #{:rating/basic :rating/bronze :rating/gold :rating/premier :rating/silver :rating/platinum}

(defmulti greet-user :rating)
;=> #'ch04.core/greet-user
(defmethod greet-user :rating/basic [user]
  (str "Hello " (:login user) \.))
;=> #object[clojure.lang.MultiFn 0x429313c2 "clojure.lang.MultiFn@429313c2"]
(defmethod greet-user :rating/premier [user]
  (str "Welcome, " (:login user) ", valued affiliate member!"))
;=> #object[clojure.lang.MultiFn 0x429313c2 "clojure.lang.MultiFn@429313c2"]
(map greet-user [user-1 user-2 user-3 user-4])
;=>
;("Hello rob."
;  "Hello gordon."
;  "Welcome, kyle, valued affiliate member!"
;  "Welcome, celeste, valued affiliate member!")

;; SUBTYPES AND MULTIPLE-DISPATCH
(remove-method profit-based-affiliate-fee ["mint.com" :rating/gold])
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(remove-method profit-based-affiliate-fee ["mint.com" :rating/platinum])
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(remove-method profit-based-affiliate-fee ["google.com" :rating/gold])
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(remove-method profit-based-affiliate-fee ["google.com" :rating/platinum])
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(defmethod profit-based-affiliate-fee ["mint.com" :rating/premier]
  [user] (fee-amount 0.05M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(defmethod profit-based-affiliate-fee ["google.com" :rating/premier]
  [user] (fee-amount 0.03M user))
;=> #object[clojure.lang.MultiFn 0x11062a4 "clojure.lang.MultiFn@11062a4"]
(map profit-based-affiliate-fee [user-1 user-2 user-3 user-4])
;=> (30.0000M 32.0000M 27.0000M 14.0000M)

;; RESOLVING METHOD AMBIGUITIES

(defmulti size-up (fn [observer observed]
                    [(:rating observer) (:rating observed)]))
;=> #'ch04.core/size-up
(defmethod size-up [:rating/platinum :rating/ANY] [_ observed]
  (str (:login observed) " seems scrawny."))
;=> #object[clojure.lang.MultiFn 0x77f4fbef "clojure.lang.MultiFn@77f4fbef"]
(defmethod size-up [:rating/ANY :rating/platinum] [_ observed]
  (str (:login observed) " shimmers with an unearthly light."))
;=> #object[clojure.lang.MultiFn 0x77f4fbef "clojure.lang.MultiFn@77f4fbef"]
size-up {:rating :rating/platinum} user-4)
;=> #object[clojure.lang.MultiFn 0x77f4fbef "clojure.lang.MultiFn@77f4fbef"]
;=> {:rating :rating/platinum}
;=> {:login "celeste", :referrer "yahoo.com", :salary 70000, :rating :rating/platinum}
;RuntimeException Unmatched delimiter: )  clojure.lang.Util.runtimeException (Util.java:221)

(prefer-method size-up [:rating/ANY :rating/platinum]
               [:rating/platinum :rating/ANY])
;=> #object[clojure.lang.MultiFn 0x77f4fbef "clojure.lang.MultiFn@77f4fbef"]
(size-up {:rating :rating/platinum} user-4)
;=> "celeste shimmers with an unearthly light."
(prefers size-up)
;=> {[:rating/ANY :rating/platinum] #{[:rating/platinum :rating/ANY]}}

;; USER-DEFINED HIERARCHIES

(def myhier (make-hierarchy))
;=> #'ch04.core/myhier
myhier
;=> {:parents {}, :descendants {}, :ancestors {}}
(derive myhier :a :letter)
;=> {:parents {:a #{:letter}}, :ancestors {:a #{:letter}}, :descendants {:letter #{:a}}}
myhier
;=> {:parents {}, :descendants {}, :ancestors {}}
(def myhier (-> myhier
                (derive :a :letter)
                (derive :b :letter)
                (derive :c :letter)))
;=> #'ch04.core/myhier
(isa? myhier :a :letter)
;=> true
(parents myhier :a)
;=> #{:letter}
(defmulti letter? identity :hierarchy #'myhier)
;=> #'ch04.core/letter?
(defmethod letter? :letter [_] true)
;=> #object[clojure.lang.MultiFn 0x57f5ade0 "clojure.lang.MultiFn@57f5ade0"]
(letter? :d)
;IllegalArgumentException No method in multimethod 'letter?' for dispatch value: :d  clojure.lang.MultiFn.getFn (MultiFn.java:156)
(def myhier (derive myhier :d :letter))
;=> #'ch04.core/myhier
(letter? :d)
;=> true

